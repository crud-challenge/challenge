package inhttp

import (
	"net/http"

	"gitlab.com/a.vandam/crud-challenge/internal/domain/services"
	"gitlab.com/a.vandam/crud-challenge/internal/logger"
)

/*GetProductsHandlerDeps is a struct definition used for storing dependencies of the GetProdByTextHTTPHandler.
In case you need to add any, add here so as to force dep inyection when invoking.*/
type GetProductsHandlerDeps struct {
	Svc services.GetProductsServiceContract
	Log logger.LogContract
}

/*CreateGetProductsHandlerFunc creates a handler function that retrieves from a query parameter a text to search*/
func CreateGetProductsHandlerFunc(dep GetProductsHandlerDeps) http.HandlerFunc {
	return http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		reqContext := req.Context()
		dep.Log.Debug("created context to obtain products")
		products, err := dep.Svc.GetProducts(reqContext)
		if err != nil {
			dep.Log.Error("received an error while fetching products: %v", err)
			http.Error(rw, wrapErrAsJSON(err), http.StatusInternalServerError)
			return
		}
		if len(*products) == 0 {
			http.Error(rw, "", http.StatusNoContent)
			return
		}
		dep.Log.Info("mapping product to response")
		dep.Log.Debug("products being mapped: %+v", products)
		response, err := mapProductsToJSONResponse(products)
		if err != nil {
			dep.Log.Error("received an error while generating response: %v", err)
			http.Error(rw, wrapErrAsJSON(err), http.StatusInternalServerError)
			return
		}
		dep.Log.Debug("response to write: %+v", string(*response))
		rw.Write(*response)
		dep.Log.Info("response has been sent back")
	})

}
