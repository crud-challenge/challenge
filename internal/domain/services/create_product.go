package services

import (
	"context"

	"gitlab.com/a.vandam/crud-challenge/internal/domain/entities"
	"gitlab.com/a.vandam/crud-challenge/internal/domain/ports"
	"gitlab.com/a.vandam/crud-challenge/internal/logger"
)

/*CreateProductService holds the dependencies needed for the service to work*/
type CreateProductService struct {
	Port ports.CreateProductPort
	Log  logger.LogContract
}

/*CreateProduct stores the service definition to get products via text fields search*/
func (svc CreateProductService) CreateProduct(ctx context.Context, prod *entities.Product) (*entities.Product, error) {
	svc.Log.Info("retrieving all products")
	prods, err := svc.Port.CreateProduct(ctx, prod)
	if err != nil {
		svc.Log.Error("port error while trying to create product:%v", err)
		return &entities.Product{}, err
	}

	return prods, err
}
