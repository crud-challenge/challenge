package inhttp

import (
	"context"
	"errors"
	"net/http"
	"net/http/httptest"
	"os"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/a.vandam/crud-challenge/internal/domain/entities"
	"gitlab.com/a.vandam/crud-challenge/internal/logger"
)

func TestUpdateProductHandler_OK(t *testing.T) {
	testCase := updateProductHandlerTestCase{
		testName: "request ok -should create a product",
		path:     "http://svctest/products",
		bodyFile: "request_bodies/update_product/update_product_ok.json",
		verb:     "POST",
		svcProdsResponse: &entities.Product{
			SKU:         "SKU 234",
			Name:        "product name",
			Brand:       "example brand",
			Size:        "23",
			Price:       100.00,
			Image:       stringsToUrls("http://test-image.com")[0],
			OtherImages: stringsToUrls("http://test-1.com", "http://test-2.com"),
		},
		expectedCode:     200,
		expectedJSONResp: "response_bodies/update_product/update_product_ok.json",
	}

	t.Run(testCase.testName, testCase.testAndAssert)
}
func TestUpdateProductHandler_NoSKUSent(t *testing.T) {
	testCase := updateProductHandlerTestCase{
		testName: "request with no sku",
		path:     "http://svctest/products",
		bodyFile: "request_bodies/update_product/update_product_no_sku.json",
		verb:     "POST",
		svcProdsResponse: &entities.Product{
			SKU:         "SKU 234",
			Name:        "product name",
			Brand:       "example brand",
			Size:        "23",
			Price:       100.00,
			Image:       stringsToUrls("http://test-image.com")[0],
			OtherImages: stringsToUrls("http://test-1.com", "http://test-2.com"),
		},
		expectedCode:     http.StatusBadRequest,
		expectedJSONResp: "response_bodies/update_product/update_product_no_sku.json",
	}

	t.Run(testCase.testName, testCase.testAndAssert)
}
func TestUpdateProductHandler_NoNameSent(t *testing.T) {
	testCase := updateProductHandlerTestCase{
		testName: "request doesn't have the name",
		path:     "http://svctest/products",
		bodyFile: "request_bodies/update_product/update_product_no_name.json",
		verb:     "POST",
		svcProdsResponse: &entities.Product{
			SKU:         "SKU 234",
			Name:        "product name",
			Brand:       "example brand",
			Size:        "23",
			Price:       100.00,
			Image:       stringsToUrls("http://test-image.com")[0],
			OtherImages: stringsToUrls("http://test-1.com", "http://test-2.com"),
		},
		svcErrResponse:   nil,
		expectedCode:     http.StatusBadRequest,
		expectedJSONResp: "response_bodies/update_product/update_product_no_name.json",
	}

	t.Run(testCase.testName, testCase.testAndAssert)
}
func TestUpdateProductHandler_NoBrandSent(t *testing.T) {
	testCase := updateProductHandlerTestCase{
		testName: "request doesn't have the brand",
		path:     "http://svctest/products",
		bodyFile: "request_bodies/update_product/update_product_no_brand.json",
		verb:     "POST",
		svcProdsResponse: &entities.Product{
			SKU:         "SKU 234",
			Name:        "product name",
			Brand:       "example brand",
			Size:        "23",
			Price:       100.00,
			Image:       stringsToUrls("http://test-image.com")[0],
			OtherImages: stringsToUrls("http://test-1.com", "http://test-2.com"),
		},
		svcErrResponse:   nil,
		expectedCode:     http.StatusBadRequest,
		expectedJSONResp: "response_bodies/update_product/update_product_no_brand.json",
	}

	t.Run(testCase.testName, testCase.testAndAssert)
}
func TestUpdateProductHandler_NoSizeSent(t *testing.T) {
	testCase := updateProductHandlerTestCase{
		testName: "request doesn't have the size",
		path:     "http://svctest/products",

		bodyFile: "request_bodies/update_product/update_product_no_size.json",
		verb:     "POST",
		svcProdsResponse: &entities.Product{
			SKU:         "SKU 234",
			Name:        "product name",
			Brand:       "example brand",
			Size:        "23",
			Price:       100.00,
			Image:       stringsToUrls("http://test-image.com")[0],
			OtherImages: stringsToUrls("http://test-1.com", "http://test-2.com"),
		},
		svcErrResponse:   nil,
		expectedCode:     http.StatusOK,
		expectedJSONResp: "response_bodies/update_product/update_product_no_size.json",
	}

	t.Run(testCase.testName, testCase.testAndAssert)
}
func TestUpdateProductHandler_EmptyProductReturned(t *testing.T) {
	testCase := updateProductHandlerTestCase{
		testName:         "request ok, but no product was returned (weird case)",
		path:             "http://svctest/products",
		bodyFile:         "request_bodies/update_product/update_product_ok.json",
		verb:             "POST",
		svcProdsResponse: &entities.Product{},
		svcErrResponse:   nil,
		expectedCode:     http.StatusInternalServerError,
		expectedJSONResp: "response_bodies/update_product/update_product_svc_empty_response.json",
	}

	t.Run(testCase.testName, testCase.testAndAssert)
}

func TestUpdateProductHandler_SvcError(t *testing.T) {
	testCase := updateProductHandlerTestCase{
		testName:         "request ok but svc error happened",
		path:             "http://svctest/products",
		bodyFile:         "request_bodies/update_product/update_product_ok.json",
		verb:             "POST",
		svcProdsResponse: &entities.Product{},
		svcErrResponse:   errors.New("a service error"),
		expectedCode:     http.StatusInternalServerError,
		expectedJSONResp: "response_bodies/update_product/update_product_svcerror.json",
	}

	t.Run(testCase.testName, testCase.testAndAssert)
}

type updateProductHandlerTestCase struct {
	testName         string
	path             string
	id               int
	verb             string
	bodyFile         string
	svcProdsResponse *entities.Product
	svcErrResponse   error
	expectedCode     int
	expectedJSONResp string
}

func (testCase updateProductHandlerTestCase) testAndAssert(t *testing.T) {
	t.Logf("testing function")

	/**---------------------- FUNCTION UNDER TEST -----------------------**/
	/*Dependencies*/
	mockedSvc := dummyUpdateProductSvc{
		product: testCase.svcProdsResponse,
		svcErr:  testCase.svcErrResponse,
	}
	loggerFactory := logger.LogFactory{LogLevel: "NONE"}
	log := loggerFactory.CreateLog("")
	dependencies := UpdateProductHandlerDeps{mockedSvc, log}
	/*END Dependencies*/
	// Test function //
	handlerFunc := CreateUpdateProductHandler(dependencies)
	/*Create test request and server*/
	var req *http.Request

	if testCase.bodyFile == "" {
		req = httptest.NewRequest(testCase.verb, testCase.path, nil)
	} else {
		bodyToSend, fileReadErr := os.ReadFile(testCase.bodyFile)
		if fileReadErr != nil {
			t.Logf("error while opening json request file: %v", fileReadErr)
			t.FailNow()
			return
		}
		reader := strings.NewReader(string(bodyToSend))
		req = httptest.NewRequest(testCase.verb, testCase.path, reader)

	}

	responseWriter := httptest.NewRecorder()
	handlerFunc(responseWriter, req)
	/*END Create test request and server*/
	/**---------------------- END FUNCTION UNDER TEST -----------------------**/
	receivedCode := responseWriter.Result().StatusCode
	if !assert.Equal(t, testCase.expectedCode, receivedCode, "difference in http code expected (%v) and obtained (%v)", testCase.expectedCode, receivedCode) {
		t.FailNow()
		return
	}
	expectedBody, bodyNotFoundErr := os.ReadFile(testCase.expectedJSONResp)
	if bodyNotFoundErr != nil {
		t.Logf("json file that stores the expected body has not been found: %v", bodyNotFoundErr)
		t.FailNow()
		return
	}
	/*var expectedBodyAsMap, receivedBodyAsMap interface{}
	json.Unmarshal(expectedBody, &expectedBodyAsMap)*/
	receivedBody := responseWriter.Body.Bytes()
	//json.Unmarshal(receivedBody, &receivedBodyAsMap)
	require.JSONEqf(t, string(receivedBody), string(expectedBody), "\n \n responses differ in testcase: %v \n \n", testCase.testName)

	t.Logf("OK!!!! - test case:  %v  - OK!!!!", testCase.testName)
}

type dummyUpdateProductSvc struct {
	product *entities.Product
	svcErr  error
}

func (mock dummyUpdateProductSvc) UpdateProduct(ctx context.Context, product *entities.Product) (*entities.Product, error) {
	return mock.product, mock.svcErr
}
