package mongodb

import (
	"context"
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/a.vandam/crud-challenge/internal/domain/entities"
	"gitlab.com/a.vandam/crud-challenge/internal/logger"
)

func TestUpdateProductAdapter_UpdateOK(t *testing.T) {
	testCase := updateProductAdapterTestCase{
		testname: "creation of one product OK",
		ctxArg:   context.TODO(),
		productArg: entities.Product{
			SKU:         "test sku",
			Name:        "test name",
			Brand:       "brand",
			Size:        "talle m",
			Image:       stringsToUrl([]string{"https://example.com"})[0],
			Price:       123.5,
			OtherImages: stringsToUrl([]string{"www.image_uri.com", "www.image_dos.com"}),
		},
		wrapperReturned: updateProductDAO{
			Sku:         "test sku",
			Name:        "test name",
			Brand:       "brand",
			Size:        "talle m",
			Price:       123.5,
			ImageURL:    "https://example.com",
			OtherImages: []string{"www.image_uri.com", "www.image_dos.com"},
		},
		wrapperError: nil,
		expected: entities.Product{
			SKU:         "test sku",
			Name:        "test name",
			Brand:       "brand",
			Size:        "talle m",
			Image:       stringsToUrl([]string{"https://example.com"})[0],
			Price:       123.5,
			OtherImages: stringsToUrl([]string{"www.image_uri.com", "www.image_dos.com"}),
		},
	}
	testCase.Run(t)
}

func TestUpdateProductAdapter_DBError(t *testing.T) {
	testCase := updateProductAdapterTestCase{
		testname:        "update failed due to with error",
		ctxArg:          context.TODO(),
		wrapperReturned: updateProductDAO{},
		wrapperError:    errors.New("a generic db error"),
		expected:        entities.Product{},
		expectedErrMsg:  "a generic db error",
	}
	testCase.Run(t)
}

type updateProductAdapterTestCase struct {
	testname        string
	ctxArg          context.Context
	productArg      entities.Product
	wrapperReturned updateProductDAO
	wrapperError    error
	expected        entities.Product
	expectedErrMsg  string
}

func (tc updateProductAdapterTestCase) Run(t *testing.T) {
	t.Run(tc.testname, func(t *testing.T) {

		logFactory := logger.LogFactory{LogLevel: "info"}
		logger := logFactory.CreateLog("debug")
		adapter := UpdateProductAdapter{
			Database:   "mock",
			Collection: "collection",
			Client: &dummyClientWrapper{
				updateProdResponse: tc.wrapperReturned,
				mockedError:        tc.wrapperError,
			},
			Log: logger,
		}
		got, err := adapter.UpdateOne(tc.ctxArg, &tc.productArg)
		if err != nil && err.Error() != tc.expectedErrMsg {
			assert.EqualError(t, err, tc.expectedErrMsg, "error don't match")
			return
		}
		if !assert.Equal(t, *got, tc.expected) {
			t.Errorf("UpdateProducts.UpdateProduct() = %v, want %v", *got, tc.expected)
			t.Fail()

		}
	})
}
