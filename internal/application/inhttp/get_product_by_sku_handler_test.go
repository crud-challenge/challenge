package inhttp

import (
	"context"
	"encoding/json"
	"errors"
	"net/http"
	"net/http/httptest"
	"net/url"
	"os"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/a.vandam/crud-challenge/internal/domain/entities"
	"gitlab.com/a.vandam/crud-challenge/internal/logger"
)

const productsPath string = "http://svctest/products"

func TestGetProductBySkuHandler_OK(t *testing.T) {
	testCase := getProdByskuHandlerTestCase{
		testName: "request with sku sent in path param",
		path:     productsPath + "/sku-234",
		bodyFile: "",

		verb: "GET",
		svcProdsResponse: &entities.Product{
			SKU:         "SKU-234",
			Name:        "product name",
			Brand:       "example brand",
			Size:        "23",
			Price:       100.00,
			Image:       stringsToUrls("http://test-image.com")[0],
			OtherImages: stringsToUrls("http://test-1.com", "http://test-2.com")},
		expectedCode:     200,
		expectedJSONResp: "response_bodies/get_product_by_sku/get_product_by_sku_ok.json",
	}

	t.Run(testCase.testName, testCase.testAndAssert)
}
func TestGetProductBySkuHandler_NoSKUSent(t *testing.T) {
	testCase := getProdByskuHandlerTestCase{
		testName: "request with no sku sent in path param",
		path:     productsPath + "/",
		bodyFile: "",
		verb:     "GET",
		svcProdsResponse: &entities.Product{
			SKU:         "SKU 234",
			Name:        "product name",
			Brand:       "example brand",
			Size:        "23",
			Price:       100.00,
			Image:       url.URL{Path: "http://test-image.com"},
			OtherImages: []url.URL{{Path: "http://test-1.com"}, {Path: "http://test-1.com"}},
		},
		expectedCode:     http.StatusBadRequest,
		expectedJSONResp: "response_bodies/get_product_by_sku/get_product_by_sku_no_sku_sent.json",
	}

	t.Run(testCase.testName, testCase.testAndAssert)
}

func TestGetProductBySkuHandler_EmptyProductReturned(t *testing.T) {
	testCase := getProdByskuHandlerTestCase{
		testName: "request with path param sent but no prods found",
		path:     productsPath + "/sku-234",
		bodyFile: "",
		verb:     "GET",
		svcProdsResponse: &entities.Product{
			SKU: "",
		},
		svcErrResponse:   nil,
		expectedCode:     http.StatusNoContent,
		expectedJSONResp: "response_bodies/get_product_by_sku/get_product_by_sku_no_product.json",
	}

	t.Run(testCase.testName, testCase.testAndAssert)
}

func TesGetProductBySKU_SvcError(t *testing.T) {
	testCase := getProdByskuHandlerTestCase{
		testName:         "request with path param sent but svc threw error",
		path:             productsPath + "/sku-234",
		bodyFile:         "",
		verb:             "GET",
		svcProdsResponse: &entities.Product{},
		svcErrResponse:   errors.New("a service error"),
		expectedCode:     http.StatusInternalServerError,
		expectedJSONResp: "response_bodies/get_product_by_sku/get_product_by_sku_svcerror.json",
	}

	t.Run(testCase.testName, testCase.testAndAssert)
}

type getProdByskuHandlerTestCase struct {
	testName         string
	path             string
	id               int
	verb             string
	bodyFile         string
	svcProdsResponse *entities.Product
	svcErrResponse   error
	expectedCode     int
	expectedJSONResp string
}

func (testCase getProdByskuHandlerTestCase) testAndAssert(t *testing.T) {
	t.Logf("testing function")

	/**---------------------- FUNCTION UNDER TEST -----------------------**/
	/*Dependencies*/
	mockedSvc := dummyGetProductBySkuSvc{
		product: testCase.svcProdsResponse,
		svcErr:  testCase.svcErrResponse,
	}
	loggerFactory := logger.LogFactory{LogLevel: "INFO"}
	log := loggerFactory.CreateLog("")
	dependencies := GetProductByIDHandlerDeps{mockedSvc, log}
	/*END Dependencies*/
	// Test function //
	handlerFunc := CreateGetProdByIDHandlerFunc(dependencies)

	/*Create test request and server*/
	var req *http.Request

	if testCase.bodyFile == "" {
		req = httptest.NewRequest(testCase.verb, testCase.path, nil)
	} else {
		bodyToSend, fileReadErr := os.ReadFile(testCase.bodyFile)
		if fileReadErr != nil {
			t.Logf("error while opening json request file: %v", fileReadErr)
			t.FailNow()
			return
		}
		reader := strings.NewReader(string(bodyToSend))
		req = httptest.NewRequest(testCase.verb, testCase.path, reader)
	}
	pathParam := strings.Replace(testCase.path, productsPath+"/", "", 2)
	pathParamCtx := context.WithValue(req.Context(), "sku", pathParam)
	req = req.Clone(pathParamCtx)

	responseWriter := httptest.NewRecorder()
	handlerFunc.ServeHTTP(responseWriter, req)
	/*END Create test request and server*/
	/**---------------------- END FUNCTION UNDER TEST -----------------------**/
	receivedCode := responseWriter.Result().StatusCode
	if !assert.Equal(t, testCase.expectedCode, receivedCode, "difference in http code expected (%v) and obtained (%v)", testCase.expectedCode, receivedCode) {
		t.FailNow()
		return
	}
	expectedBody, bodyNotFoundErr := os.ReadFile(testCase.expectedJSONResp)
	if bodyNotFoundErr != nil {
		t.Logf("json file that stores the expected body has not been found: %v", bodyNotFoundErr)
		t.FailNow()
		return
	}
	var expectedBodyAsMap, receivedBodyAsMap interface{}
	json.Unmarshal(expectedBody, &expectedBodyAsMap)
	receivedBody := responseWriter.Body.Bytes()
	json.Unmarshal(receivedBody, &receivedBodyAsMap)

	if !assert.Equal(t, expectedBodyAsMap, receivedBodyAsMap, "difference in body expected (%v) and obtained (%v)", expectedBodyAsMap, receivedBodyAsMap) {
		t.FailNow()
		return
	}

	t.Logf("OK!!!! - test case:  %v  - OK!!!!", testCase.testName)
}

type dummyGetProductBySkuSvc struct {
	product *entities.Product
	svcErr  error
}

func (mock dummyGetProductBySkuSvc) GetProductBySKU(ctx context.Context, sku string) (*entities.Product, error) {
	return mock.product, mock.svcErr
}
