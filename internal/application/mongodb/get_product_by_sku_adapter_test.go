package mongodb

import (
	"context"
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/a.vandam/crud-challenge/internal/domain/entities"
	"gitlab.com/a.vandam/crud-challenge/internal/logger"
)

func TestGetProductBySKUAdapter_GetProduct(t *testing.T) {
	testCase := getProductBySKUTestCase{
		testname: "fetch OK of one product",
		ctxArg:   context.TODO(),
		wrapperReturned: getProductDAO{
			Sku:         "SKU 234",
			Name:        "product name",
			Brand:       "example brand",
			Size:        "23",
			Price:       100.00,
			ImageURL:    "http://test-image.com",
			OtherImages: []string{"http://test-1.com", "http://test-2.com"},
		},
		wrapperError: nil,
		expected: entities.Product{
			SKU:         "SKU 234",
			Name:        "product name",
			Brand:       "example brand",
			Size:        "23",
			Price:       100.00,
			Image:       stringsToUrl([]string{"http://test-image.com"})[0],
			OtherImages: stringsToUrl([]string{"http://test-1.com", "http://test-2.com"}),
		},
	}
	testCase.Run(t)
}

func TestGetProductBySKUAdapter_DBError(t *testing.T) {
	testCase := getProductBySKUTestCase{
		testname:        "fetch failed due to with error",
		ctxArg:          context.TODO(),
		wrapperReturned: getProductDAO{},
		wrapperError:    errors.New("a generic db error"),
		expected:        entities.Product{},
		expectedErrMsg:  "a generic db error",
	}
	testCase.Run(t)
}

type getProductBySKUTestCase struct {
	testname        string
	ctxArg          context.Context
	skuUsed         string
	wrapperReturned getProductDAO
	wrapperError    error
	expected        entities.Product
	expectedErrMsg  string
}

func (tc getProductBySKUTestCase) Run(t *testing.T) {
	t.Run(tc.testname, func(t *testing.T) {

		logFactory := logger.LogFactory{LogLevel: "info"}
		logger := logFactory.CreateLog("debug")
		adapter := GetProductBySKUAdapter{
			Database:   "mock",
			Collection: "collection",
			Client: &dummyClientWrapper{
				getProdBYSKUResponse: tc.wrapperReturned,
				mockedError:          tc.wrapperError,
			},
			Log: logger,
		}
		got, err := adapter.GetProductBySKU(tc.ctxArg, tc.skuUsed)
		if err != nil && err.Error() != tc.expectedErrMsg {
			assert.EqualError(t, err, tc.expectedErrMsg, "error don't match")
			return
		}
		if !assert.Equal(t, *got, tc.expected) {
			t.Errorf("GetProductBySKUAdapter.GetProducts() = %v, want %v", got, tc.expected)
			t.Fail()
		}
	})
}
