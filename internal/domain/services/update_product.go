package services

import (
	"context"

	"gitlab.com/a.vandam/crud-challenge/internal/domain/entities"
	"gitlab.com/a.vandam/crud-challenge/internal/domain/ports"
	"gitlab.com/a.vandam/crud-challenge/internal/logger"
)

/*GetProductsService holds the dependencies needed for the service to work*/
type UpdateProductService struct {
	Port ports.UpdateProductPort
	Log  logger.LogContract
}

/*GetProductsByText stores the service definition to get products via text fields search*/
func (svc UpdateProductService) UpdateProduct(ctx context.Context, prod *entities.Product) (*entities.Product, error) {
	svc.Log.Info("updating product")
	prod, err := svc.Port.UpdateOne(ctx, prod)
	if err != nil {
		svc.Log.Error("port error while trying to create product:%v", err)
		return &entities.Product{}, err
	}
	svc.Log.Info("product updated")
	return prod, nil
}
