package services

import (
	"context"

	"gitlab.com/a.vandam/crud-challenge/internal/domain/ports"
	"gitlab.com/a.vandam/crud-challenge/internal/logger"
)

/*DeleteProductService holds the dependencies needed for the service to work*/
type DeleteProductService struct {
	Port ports.DeleteProductPort
	Log  logger.LogContract
}

/*DeleteProduct stores the service definition to delete products using SKU*/
func (svc DeleteProductService) DeleteProduct(ctx context.Context, sku string) error {
	svc.Log.Info("deleting product with sku: %v", sku)
	err := svc.Port.DeleteProductBySKU(ctx, sku)
	if err != nil {
		svc.Log.Error("port error while trying to delete product:%v", err)
		return err
	}
	return nil
}
