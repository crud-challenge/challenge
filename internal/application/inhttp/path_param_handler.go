package inhttp

import (
	"context"
	"net/http"

	"github.com/go-chi/chi"
	"gitlab.com/a.vandam/crud-challenge/internal/logger"
)

func SkuPathParamHandler(log logger.LogContract, next http.Handler) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		sku := chi.URLParam(r, "sku")
		if sku == "" {
			log.Info("bad request, sku is isvalid")
			w.WriteHeader(http.StatusBadRequest)
		}
		ctx := r.Context()
		ctx = context.WithValue(ctx, "sku", sku)
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

type pathParamKey struct{}
