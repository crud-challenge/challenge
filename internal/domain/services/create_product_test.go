package services

import (
	"context"
	"errors"
	"net/url"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/a.vandam/crud-challenge/internal/domain/entities"
	"gitlab.com/a.vandam/crud-challenge/internal/logger"
)

func TestCreateProduct_CreateAProduct(t *testing.T) {
	testCase := createProductTestCase{
		testName: "create a product",
		productToCreate: &entities.Product{
			SKU:         "SKU 234",
			Name:        "product name",
			Brand:       "example brand",
			Size:        "23",
			Price:       100.00,
			Image:       url.URL{Path: "http://test-image.com"},
			OtherImages: []url.URL{url.URL{Path: "http://test-1.com"}, url.URL{Path: "http://test-1.com"}},
		},
		productReturned: &entities.Product{
			SKU:         "SKU 234",
			Name:        "product name",
			Brand:       "example brand",
			Size:        "23",
			Price:       100.00,
			Image:       url.URL{Path: "http://test-image.com"},
			OtherImages: []url.URL{url.URL{Path: "http://test-1.com"}, url.URL{Path: "http://test-1.com"}},
		},
		expectedProd: &entities.Product{
			SKU:         "SKU 234",
			Name:        "product name",
			Brand:       "example brand",
			Size:        "23",
			Price:       100.00,
			Image:       url.URL{Path: "http://test-image.com"},
			OtherImages: []url.URL{url.URL{Path: "http://test-1.com"}, url.URL{Path: "http://test-1.com"}},
		},
		expectedErr: "",
	}

	t.Run(testCase.testName, testCase.testAndAssert)
}

func TestCreateProduct_PortError(t *testing.T) {
	testCase := createProductTestCase{
		testName: "port error while creating product",
		productToCreate: &entities.Product{
			SKU:         "SKU 234",
			Name:        "product name",
			Brand:       "example brand",
			Size:        "23",
			Price:       100.00,
			Image:       url.URL{Path: "http://test-image.com"},
			OtherImages: []url.URL{url.URL{Path: "http://test-1.com"}, url.URL{Path: "http://test-1.com"}},
		},
		productReturned: &entities.Product{},
		errorPortInMock: errors.New("port error"),
		expectedProd:    &entities.Product{},
		expectedErr:     "port error",
	}

	t.Run(testCase.testName, testCase.testAndAssert)
}

// End test cases

type createProductTestCase struct {
	testName        string
	productToCreate *entities.Product
	productReturned *entities.Product
	errorPortInMock error
	expectedProd    *entities.Product
	expectedErr     string
}

func (testCase createProductTestCase) testAndAssert(t *testing.T) {
	t.Logf("testing function")

	/**---------------------- FUNCTION UNDER TEST -----------------------**/
	/*Dependencies*/
	mockedPort := dummyCreateProductPort{
		prodResponse: testCase.productReturned,
		err:          testCase.errorPortInMock,
	}
	loggerFactory := logger.LogFactory{LogLevel: "INFO"}
	svc := CreateProductService{
		Port: mockedPort,
		Log:  loggerFactory.CreateLog(""),
	}
	/*END Dependencies*/
	testCtx := context.Background()
	product, err := svc.CreateProduct(testCtx, testCase.productToCreate)
	/**---------------------- END FUNCTION UNDER TEST -----------------------**/
	expected := testCase.expectedProd
	if !assert.Equal(t, expected, product, "difference in value expected (%v) and obtained (%v)", testCase.expectedProd, product) {
		t.Fail()
	}
	if testCase.expectedErr != "" && err == nil {
		t.Logf("test failed as the function did not return an expected error: %v vs %v", err, testCase.expectedErr)
		t.FailNow()
	}
	if testCase.expectedErr == "" && err != nil {
		t.Logf("test failed as the function returned an error when it shouldn't: %v", err)
		t.FailNow()
	}
	if testCase.expectedErr != "" && err != nil {
		//comparing errors
		if !assert.EqualErrorf(t, err, testCase.expectedErr, "function returned an unexpected error: expected: %v vs found: %v", testCase.expectedErr, err) {
			t.FailNow()
		}
	}

	t.Logf("OK!!!! - test case:  %v  - OK!!!!", testCase.testName)
}

/*Mocking port*/
type dummyCreateProductPort struct {
	prodResponse *entities.Product
	err          error
}

/*Mocking port's functions*/
func (mock dummyCreateProductPort) CreateProduct(ctx context.Context, product *entities.Product) (*entities.Product, error) {
	return mock.prodResponse, mock.err
}
