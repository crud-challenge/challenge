package entities

import "net/url"

/*Product is an entity used to pass around and store Product information*/
type Product struct {
	SKU         string
	Name        string
	Brand       string
	Size        string
	Price       float32
	Image       url.URL
	OtherImages []url.URL
}

func (prod *Product) ImagesToString() []string {
	asStr := make([]string, len(prod.OtherImages))
	for i, url := range prod.OtherImages {
		asBytes, _ := url.MarshalBinary()

		asStr[i] = string(asBytes)
	}
	return asStr

}
