package services

import (
	"context"
	"errors"
	"net/url"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/a.vandam/crud-challenge/internal/domain/entities"
	"gitlab.com/a.vandam/crud-challenge/internal/logger"
)

func TestGetProducts_GetOneProduct(t *testing.T) {
	testCase := getProductsTestCase{
		testName: "retrieve only one product, the only found",
		returnedProdsInPortMock: []entities.Product{
			{
				SKU:         "SKU 234",
				Name:        "product name",
				Brand:       "example brand",
				Size:        "23",
				Price:       100.00,
				Image:       url.URL{Path: "http://test-image.com"},
				OtherImages: []url.URL{url.URL{Path: "http://test-1.com"}, url.URL{Path: "http://test-1.com"}},
			},
		},
		expectedProd: []entities.Product{
			{
				SKU:         "SKU 234",
				Name:        "product name",
				Brand:       "example brand",
				Size:        "23",
				Price:       100.00,
				Image:       url.URL{Path: "http://test-image.com"},
				OtherImages: []url.URL{url.URL{Path: "http://test-1.com"}, url.URL{Path: "http://test-1.com"}},
			},
		},
		expectedErr: "",
	}

	t.Run(testCase.testName, testCase.testAndAssert)
}

func TestGetProducts_GetTwoProducts(t *testing.T) {
	testCase := getProductsTestCase{
		testName: "retrieve two products",
		returnedProdsInPortMock: []entities.Product{
			{
				SKU:         "SKU 234",
				Name:        "product name",
				Brand:       "example brand",
				Size:        "23",
				Price:       100.00,
				Image:       url.URL{Path: "http://test-image.com"},
				OtherImages: []url.URL{url.URL{Path: "http://test-1.com"}, url.URL{Path: "http://test-1.com"}},
			},
			{
				SKU:         "SKU 2345",
				Name:        "product name 2",
				Brand:       "example brand 2",
				Size:        "232",
				Price:       150.00,
				Image:       url.URL{Path: "http://test-image.com"},
				OtherImages: []url.URL{url.URL{Path: "http://test-1.com"}, url.URL{Path: "http://test-1.com"}},
			},
		},
		expectedProd: []entities.Product{
			{
				SKU:         "SKU 234",
				Name:        "product name",
				Brand:       "example brand",
				Size:        "23",
				Price:       100.00,
				Image:       url.URL{Path: "http://test-image.com"},
				OtherImages: []url.URL{url.URL{Path: "http://test-1.com"}, url.URL{Path: "http://test-1.com"}},
			},
			{
				SKU:         "SKU 2345",
				Name:        "product name 2",
				Brand:       "example brand 2",
				Size:        "232",
				Price:       150.00,
				Image:       url.URL{Path: "http://test-image.com"},
				OtherImages: []url.URL{url.URL{Path: "http://test-1.com"}, url.URL{Path: "http://test-1.com"}},
			},
		},
	}

	t.Run(testCase.testName, testCase.testAndAssert)
}
func TestGetProducts_NoProductsFound(t *testing.T) {
	testCase := getProductsTestCase{

		testName:                "no products found in port",
		returnedProdsInPortMock: []entities.Product{},
		expectedProd:            []entities.Product{},
		expectedErr:             "",
	}

	t.Run(testCase.testName, testCase.testAndAssert)
}

func TestGetProducts_ErrorInPort(t *testing.T) {
	testCase := getProductsTestCase{
		testName:                "port throws error while retrieving products",
		returnedProdsInPortMock: []entities.Product{},
		expectedProd:            []entities.Product{},
		errorPortInMock:         errors.New("a generic db error"),
		expectedErr:             "a generic db error",
	}

	t.Run(testCase.testName, testCase.testAndAssert)
}

// End test cases

type getProductsTestCase struct {
	testName                string
	returnedProdsInPortMock []entities.Product
	errorPortInMock         error
	expectedProd            []entities.Product
	expectedErr             string
}

func (testCase getProductsTestCase) testAndAssert(t *testing.T) {
	t.Logf("testing function")

	/**---------------------- FUNCTION UNDER TEST -----------------------**/
	/*Dependencies*/
	mockedPort := dummyGetProductsPort{
		products: testCase.returnedProdsInPortMock,
		err:      testCase.errorPortInMock,
	}
	loggerFactory := logger.LogFactory{LogLevel: "INFO"}
	svc := GetProductsService{
		Port: mockedPort,
		Log:  loggerFactory.CreateLog(""),
	}
	/*END Dependencies*/
	testCtx := context.Background()
	product, err := svc.GetProducts(testCtx)
	/**---------------------- END FUNCTION UNDER TEST -----------------------**/
	expected := testCase.expectedProd
	if !assert.Equal(t, &expected, product, "difference in value expected (%v) and obtained (%v)", testCase.expectedProd, product) {
		t.Fail()
	}
	if testCase.expectedErr != "" && err == nil {
		t.Logf("test failed as the function did not return an expected error: %v vs %v", err, testCase.expectedErr)
		t.FailNow()
	}
	if testCase.expectedErr == "" && err != nil {
		t.Logf("test failed as the function returned an error when it shouldn't: %v", err)
		t.FailNow()
	}
	if testCase.expectedErr != "" && err != nil {
		//comparing errors
		if !assert.EqualErrorf(t, err, testCase.expectedErr, "function returned an unexpected error: expected: %v vs found: %v", testCase.expectedErr, err) {
			t.FailNow()
		}
	}

	t.Logf("OK!!!! - test case:  %v  - OK!!!!", testCase.testName)
}

/*Mocking port*/
type dummyGetProductsPort struct {
	products []entities.Product
	err      error
}

/*Mocking port's functions*/
func (mock dummyGetProductsPort) GetProducts(ctx context.Context) (*[]entities.Product, error) {
	return &mock.products, mock.err
}
