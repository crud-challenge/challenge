### CRUD Challenge ###
Tech stack:
- Go 1.18
- MongoDB Database (found here), provided by Walmart to be used as an example
- MongoDB Client for connection to the DB.
- Docker + DockerCompose + make commands.


### Project Description ###
This project is a short term challenge that preforms CRUD Operations on a Mongo DB, applying some principles that I've been learning in Go for the past few months. I wouldn't say it's a completed project, neither the best of my work: it's what I could do in the limited time.
    

### Architecture:

The layer separations are done resembling the ports+adapters architectural pattern. They follow this criteria:
- Application layer:
    - Adapters implementations, both inbound (inhttp package) and outbound (mongo db adapter and it's mongoclient which embeds). 
        - In the inhttp folder, you may find example responses and bodies. In the future this can be changed to allow .golden files, for example (would love to, but time is short)
        - In the mongodb folder, you may see the implementation of the Go mongo client. It's an attempt to decouple the wrapper from the adapters, as I've faced several problems with mocking it in the present and past (unfortunately, to this date, havent' found better solutions)
    - Infrastructure layer:
        - Configurations needed to run the project (see below)  can also be found here. Environmental variables are retrieved from this layer (to allow a basis for an infrastructure-as-code management in the future, in case you need it)
    - Domain layer:
        - This layer stores business logic (services), ports definitions (using interfaces). 

All layers are coupled using Polimorphism and dependency injection in the main.go file. 


# How to: #

After cloning the project, do yo want ...?
Note: remember to have docker installed for running db or the microservice.
## Previous configuration

An example .env is provided. All values  excepto LOG_LEVEL are **REQUIRED**

        `
        MONGO_DATABASE_PORT=27017
        MONGO_DATABASE_HOST=mongo-database
        MONGO_DATABASE_NAME=products
        MONGO_DATABASE_USER=mongo-user
        MONGO_DATABASE_PASSWORD=mongo-password
        MONGO_COLLECTION_NAME=products
        AUTH_SOURCE=admin
        LOG_LEVEL=DEBUG
        HTTP_IN_PORT=8080

        `


### Commands
* To run the project:
    - Start dockerdaemon (dockerd)
    - Run  `make start-test-env`

* To run the db:
    - Start dockerdaemon
    - Run `make start-db`
    
* To run the microservice 
    - Start DB (Must!)
    - Run `make start-svc`

* To run tests everywhere (similar to the pipeline)
    - Run `make test`

* To clean after running:
   - Run `make clean`
    This will eliminate generated files, stop docker instances. 

* To delete the DB:
    - Delete the `mongo-database`folder.

## Endpoints: ##

Note: Use curl, or import to postman.

- Create a product:


        `curl localhost:8080/products -i --data '{
                    "sku":"SKU 234",
                    "name":"product name", 
                    "brand":"example brand", 
                    "size":"23", 
                    "price":100,
                    "image":"http://test-image.com", 
                    "otherImages":["http://test-1.com", "http://test-1.com"]
                }' -X PUT`
- To Update:


        `curl localhost:8080/products -i --data '{
            "sku":"SKU 234",
            "name":"product name", 
            "brand":"example brand", 
            "size":"23", 
            "price":100,
            "image":"http://test-image.com", 
            "otherImages":["http://test-1.com", "http://test-1.com"]
        }'`

- To get all products:

        `curl localhost:8080/products -X GET`


- To get a product using SKU key:
        `curl -i -X GET 'http://localhost:8080/api/products/FAL-12345' \
        --header 'Accept: */*' `

- To delete product:

            `curl localhost:8080/products/FAL-840 -X DELETE -i`


Note: Replace localhost and 8080 if needed. 


### Pending:
- Test Coverage: around 70%, should be raised.
- Integration tests, replacing the client wrapper with the dummy one. 
- Linting and doc, my lint pipeline is begging me to stop commiting.
