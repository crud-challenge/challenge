package inhttp

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"

	"gitlab.com/a.vandam/crud-challenge/internal/domain/services"
	"gitlab.com/a.vandam/crud-challenge/internal/logger"
)

type CreateProductHandlerDeps struct {
	Svc services.CreateProductServiceContract
	Log logger.LogContract
}

func CreateCreateProductHandler(dep CreateProductHandlerDeps) http.HandlerFunc {
	return http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		dep.Log.Debug("unmarshalling request")
		decoder := json.NewDecoder(req.Body)
		var dto productJSONRequest
		err := decoder.Decode(&dto)
		defer req.Body.Close()
		if err != nil {
			dep.Log.Error("invalid body error :%v", err)
			http.Error(rw, wrapErrAsJSON(err), http.StatusBadRequest)
			req.Context().Done()
			return
		}
		dep.Log.Debug("mapping request to entity: req: %v", dto)
		product, err := mapProductJsonRequestToEntity(&dto)
		if err != nil {
			errMsg := fmt.Sprintf("request map error: %v", err)
			dep.Log.Error(errMsg)
			http.Error(rw, wrapErrAsJSON(errors.New(errMsg)), http.StatusBadRequest)
			req.Context().Done()
			return
		}
		dep.Log.Info("creating context to create product")
		reqContext := req.Context()
		createdProduct, err := dep.Svc.CreateProduct(reqContext, product)
		if err != nil {
			dep.Log.Error("received an error while creating product: %v", err)
			http.Error(rw, wrapErrAsJSON(err), http.StatusInternalServerError)
			req.Context().Done()
			return
		}
		prod := *createdProduct
		if prod.SKU == "" {
			dep.Log.Error("no product created - already exists")
			http.Error(rw, wrapErrAsJSON(errors.New("no product created - already exists")), http.StatusBadRequest)
			req.Context().Done()
			return
		}
		dep.Log.Info("mapping product to response")
		response, err := mapProductToJSONResponse(createdProduct)
		if err != nil {
			dep.Log.Error("received an error while generating response: %v", err)
			http.Error(rw, wrapErrAsJSON(err), http.StatusInternalServerError)
			req.Context().Done()
			return
		}
		dep.Log.Debug("response to write: %+v", string(*response))
		rw.WriteHeader(http.StatusCreated)
		_, err = rw.Write(*response)
		if err != nil {
			dep.Log.Error("received an error while writing response body: %v", err)
			http.Error(rw, wrapErrAsJSON(err), http.StatusInternalServerError)
			req.Context().Done()
			return
		}
		dep.Log.Info("response has been sent back")
	})

}
