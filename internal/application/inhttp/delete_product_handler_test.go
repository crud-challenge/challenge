package inhttp

import (
	"context"
	"encoding/json"
	"errors"
	"net/http"
	"net/http/httptest"
	"os"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/a.vandam/crud-challenge/internal/logger"
)

const deleteProductPath string = "http://svctest/products"

func TestDeleteProductBySkuHandler_OK(t *testing.T) {
	testCase := deleteProdByskuHandlerTestCase{
		testName:         "request with sku sent in path param",
		path:             deleteProductPath + "/SKU_234",
		bodyFile:         "",
		verb:             "DELETE",
		expectedCode:     http.StatusOK,
		expectedJSONResp: "",
	}

	t.Run(testCase.testName, testCase.testAndAssert)
}
func TestDeleteProductBySkuHandler_NoSKUSent(t *testing.T) {
	testCase := deleteProdByskuHandlerTestCase{
		testName:         "request with sku sent in path param",
		path:             deleteProductPath + "/",
		bodyFile:         "",
		verb:             "DELETE",
		expectedCode:     http.StatusBadRequest,
		expectedJSONResp: "",
	}

	t.Run(testCase.testName, testCase.testAndAssert)
}

func TestDeleteProductBySKU_SvcError(t *testing.T) {
	testCase := deleteProdByskuHandlerTestCase{
		testName:         "request with path param sent but svc threw error",
		path:             deleteProductPath + "/9999",
		bodyFile:         "",
		verb:             "DELETE",
		svcErrResponse:   errors.New("a service error"),
		expectedCode:     http.StatusInternalServerError,
		expectedJSONResp: "response_bodies/delete_product_by_sku/delete_product_by_sku_svcerror.json",
	}

	t.Run(testCase.testName, testCase.testAndAssert)
}

type deleteProdByskuHandlerTestCase struct {
	testName         string
	path             string
	id               int
	verb             string
	sku              string
	bodyFile         string
	svcErrResponse   error
	expectedCode     int
	expectedJSONResp string
}

func (testCase deleteProdByskuHandlerTestCase) testAndAssert(t *testing.T) {
	t.Logf("testing function")

	/**---------------------- FUNCTION UNDER TEST -----------------------**/
	/*Dependencies*/
	mockedSvc := dummyDeleteProductBySkuSvc{
		svcErr: testCase.svcErrResponse,
	}
	loggerFactory := logger.LogFactory{LogLevel: "INFO"}
	log := loggerFactory.CreateLog("")
	dependencies := DeleteProductByIDHandlerDeps{mockedSvc, log}
	/*END Dependencies*/
	// Test function //
	handlerFunc := CreateDeleteProdByIDHandlerFunc(dependencies)

	/*Create test request and server*/
	var req *http.Request
	requestContext := context.TODO()
	requestContext = context.WithValue(requestContext, "sku", testCase.sku)

	if testCase.bodyFile == "" {
		req = httptest.NewRequest(testCase.verb, testCase.path, nil)
	} else {
		bodyToSend, fileReadErr := os.ReadFile(testCase.bodyFile)
		if fileReadErr != nil {
			t.Logf("error while opening json request file: %v", fileReadErr)
			t.FailNow()
			return
		}
		reader := strings.NewReader(string(bodyToSend))
		req = httptest.NewRequest(testCase.verb, testCase.path, reader)

	}
	pathParam := strings.Replace(testCase.path, deleteProductPath+"/", "", 2)
	pathParamCtx := context.WithValue(req.Context(), "sku", pathParam)
	req = req.Clone(pathParamCtx)

	responseWriter := httptest.NewRecorder()
	handlerFunc.ServeHTTP(responseWriter, req)
	/*END Create test request and server*/
	/**---------------------- END FUNCTION UNDER TEST -----------------------**/
	receivedCode := responseWriter.Result().StatusCode
	if !assert.Equal(t, testCase.expectedCode, receivedCode, "difference in http code expected (%v) and obtained (%v)", testCase.expectedCode, receivedCode) {
		t.FailNow()
		return
	}
	var expectedBodyAsMap, receivedBodyAsMap interface{}
	if testCase.expectedJSONResp != "" {
		expectedBody, bodyNotFoundErr := os.ReadFile(testCase.expectedJSONResp)
		if bodyNotFoundErr != nil {
			t.Logf("json file that stores the expected body has not been found: %v", bodyNotFoundErr)
			t.FailNow()
			return
		}
		json.Unmarshal(expectedBody, &expectedBodyAsMap)
	} else {
		expectedBodyAsMap = nil
	}
	receivedBody := responseWriter.Body.Bytes()
	json.Unmarshal(receivedBody, &receivedBodyAsMap)

	if !assert.Equal(t, expectedBodyAsMap, receivedBodyAsMap, "difference in body expected (%v) and obtained (%v)", expectedBodyAsMap, receivedBodyAsMap) {
		t.FailNow()
		return
	}

	t.Logf("OK!!!! - test case:  %v  - OK!!!!", testCase.testName)
}

type dummyDeleteProductBySkuSvc struct {
	svcErr error
}

func (mock dummyDeleteProductBySkuSvc) DeleteProduct(ctx context.Context, sku string) error {
	return mock.svcErr
}
