package main

import (
	"context"
	"errors"
	"fmt"
	"net/http"
	"os"

	"github.com/go-chi/chi"
	"gitlab.com/a.vandam/crud-challenge/internal/application/inhttp"
	"gitlab.com/a.vandam/crud-challenge/internal/application/mongodb"
	"gitlab.com/a.vandam/crud-challenge/internal/domain/services"
	"gitlab.com/a.vandam/crud-challenge/internal/infrastructure/configs"
	"gitlab.com/a.vandam/crud-challenge/internal/logger"
)

func init() {

}

var log logger.LogContract

func main() {

	fmt.Println("Starting the application...")
	logConfigs, err := configs.GetLogConfigs()
	logFactory := logger.LogFactory{LogLevel: logConfigs.LogLevel}
	log = logFactory.CreateLog("main")
	if err != nil {
		log.Error("failed to start application ")
		os.Exit(1)
	}
	dbConfigs, dbConfigErr := configs.GetProductsDatabaseConfigs()
	if dbConfigErr != nil {
		errMsg := fmt.Sprintf("failed to retrieve db configurations: %v ", dbConfigErr)
		log.Error(errMsg)
		os.Exit(1)
	}
	mongoDbConnector, err := startDBOrFail(logFactory.CreateLog(""), dbConfigs)
	defer mongoDbConnector.Disconnect(context.Background())
	if err != nil {
		log.Error("impossible to start DB connector, exiting application: %v", err)
		os.Exit(1)
	}
	log := logFactory.CreateLog("")

	handlers := startDependencies(&logFactory, mongoDbConnector, dbConfigs)

	router := chi.NewRouter()
	router.Route("/products", func(router chi.Router) {
		router.Get("/", handlers.getProductsHandlerFunc)
		router.Get("/{sku}", inhttp.SkuPathParamHandler(logFactory.CreateLog("path param middle ware"), handlers.getProductBySkuHandlerFunc))
		router.Put("/", handlers.createProdHandlerFunc)
		router.Post("/", handlers.updateProductHandlerFunc)
		router.Delete("/{sku}", inhttp.SkuPathParamHandler(logFactory.CreateLog("path param middle ware"), handlers.deleteProductHandlerFunc))
	})

	err = http.ListenAndServe(":8080", router)
	if err != nil {
		log.Error("listen and serve failed: %v", err)
		os.Exit(1)
	}
	log.Info("serving http on port 8080")

}
func startDBOrFail(dbLog logger.LogContract, dbConfig *configs.ProductsDBConfigurations) (*mongodb.ClientWrapper, error) {

	mongoWrapper := &mongodb.ClientWrapper{Log: dbLog}
	mongoConn, dbConnErr := mongoWrapper.Connect(context.Background(), dbConfig)
	if dbConnErr != nil {
		errMsg := fmt.Sprintf("failed to connect  to db: %v ", dbConnErr)
		dbLog.Error(errMsg)
		return &mongodb.ClientWrapper{}, errors.New(errMsg)
	}
	pingErr := mongoConn.Ping()
	if pingErr != nil {
		errMsg := fmt.Sprintf("failed to ping  db: %v ", pingErr)
		dbLog.Error("%v", errMsg)
		return &mongodb.ClientWrapper{}, errors.New(errMsg)
	}
	dbLog.Debug("--- ping ok ---")
	return mongoConn, nil
}

type handlers struct {
	createProdHandlerFunc      http.HandlerFunc
	getProductBySkuHandlerFunc http.HandlerFunc
	getProductsHandlerFunc     http.HandlerFunc
	deleteProductHandlerFunc   http.HandlerFunc
	updateProductHandlerFunc   http.HandlerFunc
}

func startDependencies(logF *logger.LogFactory, mongoWrapper *mongodb.ClientWrapper, dbConfigs *configs.ProductsDBConfigurations) *handlers {
	createProductAdapter := mongodb.CreateProductAdapter{
		Database:   dbConfigs.MongoDatabaseName,
		Collection: dbConfigs.MongoCollectionName,
		Client:     mongoWrapper,
		Log:        logF.CreateLog(""),
	}
	createProductService := services.CreateProductService{
		Port: &createProductAdapter,
		Log:  logF.CreateLog(""),
	}
	createProductHandler := inhttp.CreateCreateProductHandler(
		inhttp.CreateProductHandlerDeps{
			Svc: createProductService,
			Log: logF.CreateLog(""),
		},
	)
	log.Info("create product handler created")

	getProductBySkuAdapter := mongodb.GetProductBySKUAdapter{
		Database:   dbConfigs.MongoDatabaseName,
		Collection: dbConfigs.MongoCollectionName,
		Client:     mongoWrapper,
		Log:        log,
	}
	getProductBySkuService := services.GetProductBySKUService{
		Port: &getProductBySkuAdapter,
		Log:  logF.CreateLog(""),
	}
	getProductBySkuHandler := inhttp.CreateGetProdByIDHandlerFunc(
		inhttp.GetProductByIDHandlerDeps{
			Svc: &getProductBySkuService,
			Log: logF.CreateLog(""),
		},
	)

	log.Info("get product handler by sku created")

	getProductsAdapter := mongodb.GetProductsAdapter{
		Database:   dbConfigs.MongoDatabaseName,
		Collection: dbConfigs.MongoCollectionName,
		Client:     mongoWrapper,
		Log:        log,
	}
	getProductsService := services.GetProductsService{
		Port: &getProductsAdapter,
		Log:  logF.CreateLog(""),
	}
	getProductsHandler := inhttp.CreateGetProductsHandlerFunc(
		inhttp.GetProductsHandlerDeps{
			Svc: &getProductsService,
			Log: logF.CreateLog(""),
		},
	)

	log.Info("get products by sku created")

	updateProductAdapter := mongodb.UpdateProductAdapter{
		Database:   dbConfigs.MongoDatabaseName,
		Collection: dbConfigs.MongoCollectionName,
		Client:     mongoWrapper,
		Log:        log,
	}
	updateProductService := services.UpdateProductService{
		Port: &updateProductAdapter,
		Log:  logF.CreateLog(""),
	}
	updateProductHandler := inhttp.CreateUpdateProductHandler(
		inhttp.UpdateProductHandlerDeps{
			Svc: &updateProductService,
			Log: logF.CreateLog(""),
		},
	)
	log.Info("update products handler created")

	deleteProductAdapter := mongodb.DeleteProductAdapter{
		Database:   dbConfigs.MongoDatabaseName,
		Collection: dbConfigs.MongoCollectionName,
		Client:     mongoWrapper,
		Log:        log,
	}
	deleteProductService := services.DeleteProductService{
		Port: &deleteProductAdapter,
		Log:  logF.CreateLog(""),
	}
	deleteProductHandler := inhttp.CreateDeleteProdByIDHandlerFunc(
		inhttp.DeleteProductByIDHandlerDeps{
			Svc: &deleteProductService,
			Log: logF.CreateLog(""),
		},
	)
	log.Info("delete products handler created")
	httpHandlers := handlers{
		createProdHandlerFunc:      createProductHandler,
		getProductBySkuHandlerFunc: getProductBySkuHandler,
		getProductsHandlerFunc:     getProductsHandler,
		deleteProductHandlerFunc:   deleteProductHandler,
		updateProductHandlerFunc:   updateProductHandler,
	}

	return &httpHandlers

}
