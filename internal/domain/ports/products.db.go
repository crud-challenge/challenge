package ports

import (
	"context"

	"gitlab.com/a.vandam/crud-challenge/internal/domain/entities"
)

/*GetProductBySKUPort is the port definition to get a product via id*/
type GetProductBySKUPort interface {
	GetProductBySKU(ctx context.Context, sku string) (*entities.Product, error)
}

/*GetProductsPort is the Port contract to get a product by looking for text within it*/
type GetProductsPort interface {
	GetProducts(ctx context.Context) (*[]entities.Product, error)
}

type CreateProductPort interface {
	CreateProduct(ctx context.Context, product *entities.Product) (*entities.Product, error)
}

type UpdateProductPort interface {
	UpdateOne(ctx context.Context, product *entities.Product) (*entities.Product, error)
}

type DeleteProductPort interface {
	DeleteProductBySKU(ctx context.Context, sku string) error
}
