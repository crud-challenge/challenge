package inhttp

import (
	"net/http"

	"gitlab.com/a.vandam/crud-challenge/internal/domain/services"
	"gitlab.com/a.vandam/crud-challenge/internal/logger"
)

type DeleteProductByIDHandlerDeps struct {
	Svc services.DeleteProductServiceContract
	Log logger.LogContract
}

func CreateDeleteProdByIDHandlerFunc(dep DeleteProductByIDHandlerDeps) http.HandlerFunc {
	return http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		skuReceived := req.Context().Value("sku").(string)
		dep.Log.Info("sku received:%v", skuReceived)
		if skuReceived == "" {
			http.Error(rw, "", http.StatusBadRequest)
			return
		}
		reqContext := req.Context()

		dep.Log.Debug("created context to obtain products")
		err := dep.Svc.DeleteProduct(reqContext, skuReceived)
		if err != nil {
			dep.Log.Error("received an error while deleting product by sku: %v", err)
			http.Error(rw, wrapErrAsJSON(err), http.StatusInternalServerError)
			return
		}
		rw.WriteHeader(http.StatusOK)
		dep.Log.Info("response has been sent back")
	})

}
