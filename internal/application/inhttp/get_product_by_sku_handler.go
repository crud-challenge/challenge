package inhttp

import (
	"net/http"

	"gitlab.com/a.vandam/crud-challenge/internal/domain/services"
	"gitlab.com/a.vandam/crud-challenge/internal/logger"
)

type GetProductByIDHandlerDeps struct {
	Svc services.GetProductBySKUServiceContract
	Log logger.LogContract
}

const skuQueryParamKey string = "sku"

func CreateGetProdByIDHandlerFunc(dep GetProductByIDHandlerDeps) http.HandlerFunc {
	return http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		skuReceived := req.Context().Value("sku").(string)
		dep.Log.Info("sku received:%v", skuReceived)
		if skuReceived == "" {
			http.Error(rw, "", http.StatusBadRequest)
			return
		}
		reqContext := req.Context()
		dep.Log.Debug("created context to obtain products")
		product, err := dep.Svc.GetProductBySKU(reqContext, skuReceived)
		if err != nil {
			dep.Log.Error("received an error while fetching product by id: %v", err)
			http.Error(rw, wrapErrAsJSON(err), http.StatusInternalServerError)
			return
		}
		if product.SKU == "" || &product == nil {
			dep.Log.Debug("no products match sent criteria: sku: %v", skuReceived)
			rw.WriteHeader(http.StatusNoContent)
			return
		}
		dep.Log.Info("mapping product to response")
		response, err := mapProductToJSONResponse(product)
		if err != nil {
			dep.Log.Error("received an error while generating response: %v", err)
			http.Error(rw, wrapErrAsJSON(err), http.StatusInternalServerError)
			return
		}
		dep.Log.Debug("response to write: %+v", string(*response))
		rw.Write(*response)
		dep.Log.Info("response has been sent back")
		req.Context().Done()
	})

}
