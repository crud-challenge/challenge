package mongodb

import (
	"context"
	"errors"
	"net/url"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func (cw ClientWrapper) FindProducts(ctx context.Context, databaseName string, collectionName string, filter bson.D) (*[]getProductDAO, error) {
	cursor, err := cw.client.Database(databaseName, options.Database()).Collection(collectionName, &options.CollectionOptions{}).Find(ctx, filter, &options.FindOptions{})
	if err != nil {
		cw.Log.Error("error while finding products: %v", err)
	}
	defer cursor.Close(ctx)
	if err != nil {
		cw.Log.Error("failed to retrieve products: %v", err)
		return &[]getProductDAO{}, errors.New("error in finds products")
	}
	cw.Log.Debug("mapping result into products DAO")
	var productsDAO []getProductDAO
	err = cursor.All(ctx, &productsDAO)
	if err != nil {
		cw.Log.Error("failed to decode products: %v", err)
		return &[]getProductDAO{}, errors.New("error in find products operation")
	}
	return &productsDAO, nil
}
func (cw ClientWrapper) FindProduct(ctx context.Context, databaseName string, collectionName string, filter bson.D) (*getProductDAO, error) {
	coll := cw.client.Database(databaseName, options.Database()).Collection(collectionName, &options.CollectionOptions{})
	singleResult := coll.FindOne(ctx, filter, &options.FindOneOptions{})
	if errors.Is(singleResult.Err(), mongo.ErrNoDocuments) {
		cw.Log.Error("no products found: %v", singleResult.Err)
		return &getProductDAO{}, nil
	}
	if singleResult.Err() != nil {
		cw.Log.Error("error while searching for product: %v", singleResult.Err)
		return &getProductDAO{}, errors.New("error in find product by sku operation")
	}
	var dao getProductDAO
	cw.Log.Debug("mapping result into DAO")
	err := singleResult.Decode(&dao)
	if err != nil {
		cw.Log.Error("failed to decode product: %v", err)
		return &getProductDAO{}, err
	}
	return &dao, nil
}

/*InsertOne attemps to insert one product into the db. Sould be noted that uses UpdateOne mongo operations, as Insert is not validating correctly uniqueness of ids.*/
func (cw ClientWrapper) InsertOne(ctx context.Context, databaseName string, collectionName string, productPtr *createProductDAO) (*createProductDAO, error) {
	coll := cw.client.Database(databaseName, options.Database()).Collection(collectionName, &options.CollectionOptions{})

	opts := options.FindOne().SetSort(bson.D{{"sku", 1}})
	var result bson.M
	err := coll.FindOne(context.TODO(), bson.D{{"sku", productPtr.Sku}}, opts).Decode(&result)
	if err != nil {
		if err == mongo.ErrNoDocuments {
			opts := options.InsertOne().SetBypassDocumentValidation(false)
			cw.Log.Debug("inserting product: %v", *productPtr)
			insertResult, err := coll.InsertOne(ctx, *productPtr, opts)
			if err != nil {
				cw.Log.Error("error while inserting product: %v", err)
				return &createProductDAO{}, errors.New("error in insert operation")
			}
			if insertResult.InsertedID == nil {
				cw.Log.Error("no product inserted")
				return &createProductDAO{}, nil
			}
			cw.Log.Debug("id generated: %+v", insertResult.InsertedID)
			product := *productPtr
			return &product, nil
		}
		cw.Log.Error("error while looking for documents: %v")
		return &createProductDAO{}, errors.New("error in validating sku for insert operation")
	}
	return &createProductDAO{}, nil

}
func (cw ClientWrapper) DeleteOne(ctx context.Context, databaseName string, collectionName string, filter bson.D) error {
	coll := cw.client.Database(databaseName, options.Database()).Collection(collectionName, &options.CollectionOptions{})
	cw.Log.Debug("deleting product")
	deleteResult := coll.FindOneAndDelete(ctx, filter)
	if errors.Is(deleteResult.Err(), mongo.ErrNoDocuments) {
		cw.Log.Info("no product found")
		return errors.New("no product deleted")
	}
	if deleteResult.Err() != nil {
		cw.Log.Error("error while deleting product: %v", deleteResult.Err)
		return errors.New("error in delete one operation")
	}
	return nil
}

func (cw ClientWrapper) UpdateOne(ctx context.Context, databaseName string, collectionName string, productPtr *updateProductDAO, filter bson.D) (*updateProductDAO, error) {
	coll := cw.client.Database(databaseName, options.Database()).Collection(collectionName, &options.CollectionOptions{})
	cw.Log.Debug("inserting product: %v", *productPtr)
	product := *productPtr

	updateResult, err := coll.ReplaceOne(ctx, filter, product, &options.ReplaceOptions{})

	if err != nil {
		cw.Log.Error("error while updating for product: %v", err)
		return &updateProductDAO{}, errors.New("error in update operation")
	}
	if updateResult.ModifiedCount == 0 {
		err = errors.New("no product updated")
		return &updateProductDAO{}, nil
	}
	return &product, nil
}

func stringsToUrl(urlsAsStr []string) []url.URL {
	urlsSlice := make([]url.URL, len(urlsAsStr))
	for i, urlStr := range urlsAsStr {
		urlsSlice[i] = url.URL{Path: urlStr}
	}
	return urlsSlice

}
