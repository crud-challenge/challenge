package inhttp

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/url"

	"gitlab.com/a.vandam/crud-challenge/internal/domain/entities"
)

/*Key used for retrieving path params from the req's context.*/
type productIDPathParamCtxKey struct {
}

const defaultParseErrorInt int = 0

const errorResponseBody string = `{"error":"%v", "resources":{}}`

func wrapErrAsJSON(err error) string {
	return fmt.Sprintf(errorResponseBody, err.Error())
}

type EmbeddingOneResourceJSONResponse struct {
	ErrMsg    string              `json:"error"`
	Resources productJSONResponse `json:"resources"`
}

type EmbeddingMultipleResourcesJSONResponse struct {
	ErrMsg    string                `json:"error"`
	Resources []productJSONResponse `json:"resources"`
}
type productJSONResponse struct {
	SKU         string   `json:"sku"`
	Name        string   `json:"name"`
	Brand       string   `json:"brand"`
	Size        string   `json:"size,omitempty"`
	Image       string   `json:"image"`
	Price       float32  `json:"price"`
	OtherImages []string `json:"otherImages,omitempty"`
}
type productJSONRequest struct {
	SKU         string   `json:"sku"`
	Name        string   `json:"name"`
	Brand       string   `json:"brand"`
	Size        string   `json:"size"`
	Image       string   `json:"image"`
	Price       float32  `json:"price"`
	OtherImages []string `json:"otherImages"`
}

func mapProductJsonRequestToEntity(dto *productJSONRequest) (*entities.Product, error) {
	if dto.SKU == "" {
		return &entities.Product{}, errors.New(`required field \"sku\" is missing`)
	}
	if dto.Name == "" {
		return &entities.Product{}, errors.New(`required field \"name\" is missing`)
	}
	if dto.Brand == "" {
		return &entities.Product{}, errors.New(`required field \"brand\" is missing`)
	}
	if dto.Image == "" {
		return &entities.Product{}, errors.New(`required field \"image\" is missing`)
	}
	if dto.Price == 0.0 {
		return &entities.Product{}, errors.New(`required field \"price\" is missing`)
	}
	imageURL, err := parseURL(dto.Image)
	if err != nil {
		return &entities.Product{}, err
	}
	otherUrls := make([]url.URL, len(dto.OtherImages))
	if len(dto.OtherImages) > 0 {
		for i, otherImgsString := range dto.OtherImages {
			otherUrls[i], err = parseURL(otherImgsString)
			if err != nil {
				return &entities.Product{}, err
			}
		}
	}
	entity := entities.Product{
		SKU:         dto.SKU,
		Name:        dto.Name,
		Brand:       dto.Brand,
		Size:        dto.Size,
		Image:       imageURL,
		Price:       dto.Price,
		OtherImages: otherUrls,
	}
	return &entity, nil
}

func mapProductToJSONResponse(prod *entities.Product) (*[]byte, error) {
	imageAsBytes, _ := prod.Image.MarshalBinary()
	imageStr := string(imageAsBytes)

	responseDTO := EmbeddingOneResourceJSONResponse{
		ErrMsg: "",
		Resources: productJSONResponse{
			SKU:         prod.SKU,
			Name:        prod.Name,
			Brand:       prod.Brand,
			Size:        prod.Size,
			Price:       prod.Price,
			Image:       imageStr,
			OtherImages: prod.ImagesToString(),
		},
	}
	responseBody, err := json.Marshal(responseDTO)

	return &responseBody, err
}
func mapProductsToJSONResponse(prodsPtr *[]entities.Product) (*[]byte, error) {
	products := *prodsPtr
	resources := make([]productJSONResponse, len(products))
	for i, prod := range products {
		resources[i] = productJSONResponse{
			SKU:         prod.SKU,
			Name:        prod.Name,
			Brand:       prod.Brand,
			Size:        prod.Size,
			Price:       prod.Price,
			Image:       prod.Image.String(),
			OtherImages: prod.ImagesToString(),
		}
	}
	response := EmbeddingMultipleResourcesJSONResponse{ErrMsg: "", Resources: resources}
	responseBody, err := json.Marshal(response)

	return &responseBody, err
}

func parseURL(urlStr string) (url.URL, error) {
	var imageUrl url.URL
	if _, err := url.ParseRequestURI(urlStr); err != nil {
		return url.URL{}, err
	}
	err := imageUrl.UnmarshalBinary([]byte(urlStr))
	if err != nil {
		return url.URL{}, err
	}
	return imageUrl, nil
}
