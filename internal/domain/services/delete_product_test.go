package services

import (
	"context"
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/a.vandam/crud-challenge/internal/logger"
)

func TestDeleteProduct_DeleteAProduct(t *testing.T) {
	testCase := deleteProductTestCase{
		testName:       "delete a product",
		productSKU:     "sku example",
		portError:      nil,
		expectedErrMsg: "",
	}

	t.Run(testCase.testName, testCase.testAndAssert)
}

func TestDeleteProduct_PortError(t *testing.T) {
	testCase := deleteProductTestCase{
		testName:       "port error while deleting product",
		productSKU:     "sku example",
		portError:      errors.New("example error"),
		expectedErrMsg: "example error",
	}

	t.Run(testCase.testName, testCase.testAndAssert)
}

// End test cases

type deleteProductTestCase struct {
	testName       string
	productSKU     string
	portError      error
	expectedErrMsg string
}

func (testCase deleteProductTestCase) testAndAssert(t *testing.T) {
	t.Logf("testing function")

	/**---------------------- FUNCTION UNDER TEST -----------------------**/
	/*Dependencies*/
	mockedPort := dummyDeleteProductPort{
		err: testCase.portError,
	}
	loggerFactory := logger.LogFactory{LogLevel: "INFO"}
	svc := DeleteProductService{
		Port: mockedPort,
		Log:  loggerFactory.CreateLog(""),
	}
	/*END Dependencies*/
	testCtx := context.Background()
	err := svc.DeleteProduct(testCtx, testCase.productSKU)
	/**---------------------- END FUNCTION UNDER TEST -----------------------**/
	if testCase.expectedErrMsg != "" {
		if !assert.EqualErrorf(t, err, testCase.expectedErrMsg, "function returned an unexpected error: expected: %v vs found: %v", testCase.expectedErrMsg, err) {
			t.FailNow()
		}
	}

	t.Logf("OK!!!! - test case:  %v  - OK!!!!", testCase.testName)
}

/*Mocking port*/
type dummyDeleteProductPort struct {
	err error
}

/*Mocking port's functions*/
func (mock dummyDeleteProductPort) DeleteProductBySKU(ctx context.Context, sku string) error {
	return mock.err
}
