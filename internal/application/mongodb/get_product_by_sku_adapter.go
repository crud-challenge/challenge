package mongodb

import (
	"context"

	"gitlab.com/a.vandam/crud-challenge/internal/domain/entities"
	"gitlab.com/a.vandam/crud-challenge/internal/logger"
	"go.mongodb.org/mongo-driver/bson"
)

type GetProductBySKUAdapter struct {
	Database   string
	Collection string
	Client     GetProductsWrapperContract
	Log        logger.LogContract
}

/*GetProductBySKU perfroms a find query with a filter and retuirns a pointer to the product found*/
func (a *GetProductBySKUAdapter) GetProductBySKU(ctx context.Context, sku string) (*entities.Product, error) {
	a.Log.Debug("trying to get products from db: %v, and collection: %v, using sku =%v", a.Database, a.Collection, sku)
	filter := bson.D{{"sku", sku}}
	daoPtr, err := a.Client.FindProduct(ctx, a.Database, a.Collection, filter)

	if err != nil {
		a.Log.Error("adapter found an error")
		return &entities.Product{}, err
	}
	if daoPtr.Sku == "" {
		a.Log.Info("no product found")
		return &entities.Product{}, err
	}
	product := entities.Product{
		SKU:         daoPtr.Sku,
		Name:        daoPtr.Name,
		Brand:       daoPtr.Brand,
		Size:        daoPtr.Size,
		Price:       daoPtr.Price,
		Image:       stringsToUrl([]string{daoPtr.ImageURL})[0],
		OtherImages: stringsToUrl(daoPtr.OtherImages),
	}
	return &product, nil
}

type getProductDAO struct {
	Sku         string   `bson:"sku,omitempty"`
	Name        string   `bson:"name,omitempty"`
	Brand       string   `bson:"brand,omitempty"`
	Size        string   `bson:"size,omitempty"`
	Price       float32  `bson:"price,omitempty"`
	ImageURL    string   `bson:"image,omitempty"`
	OtherImages []string `bson:"other,omitempty"`
}
