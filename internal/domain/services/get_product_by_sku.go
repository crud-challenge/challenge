package services

import (
	"context"

	"gitlab.com/a.vandam/crud-challenge/internal/domain/entities"
	"gitlab.com/a.vandam/crud-challenge/internal/domain/ports"
	"gitlab.com/a.vandam/crud-challenge/internal/logger"
)

/*GetProductBySKUService establishes dependencies needed for the getProductByIdservice to work*/
type GetProductBySKUService struct {
	Port ports.GetProductBySKUPort
	Log  logger.LogContract
}

/*GetProductBySKU declares the service implementation to get a product via SKU*/
func (svc GetProductBySKUService) GetProductBySKU(ctx context.Context, sku string) (*entities.Product, error) {
	svc.Log.Info("trying to look for a product with id: %v", sku)
	prod, err := svc.Port.GetProductBySKU(ctx, sku)
	return prod, err
}
