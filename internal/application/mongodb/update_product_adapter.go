package mongodb

import (
	"context"

	"gitlab.com/a.vandam/crud-challenge/internal/domain/entities"
	"gitlab.com/a.vandam/crud-challenge/internal/logger"
	"go.mongodb.org/mongo-driver/bson"
)

type UpdateProductAdapter struct {
	Database   string
	Collection string
	Client     UpdateProductWrapperContract
	Log        logger.LogContract
}

/*GetProductBySKU perfroms a find query with a filter and retuirns a pointer to the product found*/
func (a *UpdateProductAdapter) UpdateOne(ctx context.Context, product *entities.Product) (*entities.Product, error) {
	a.Log.Debug("trying to update product in db : %v, and collection: %v", a.Database, a.Collection)
	productObject := *product
	productToCreate := updateProductDAO{
		Sku:         productObject.SKU,
		Name:        productObject.Name,
		Brand:       productObject.Brand,
		Size:        productObject.Size,
		Price:       productObject.Price,
		ImageURL:    productObject.Image.String(),
		OtherImages: productObject.ImagesToString(),
	}
	filter := bson.D{{"sku", productObject.SKU}}
	daoPtr, err := a.Client.UpdateOne(ctx, a.Database, a.Collection, &productToCreate, filter)
	if err != nil {
		a.Log.Error("adapter found an error")
		return &entities.Product{}, err
	}
	dao := *daoPtr
	updatedProduct := entities.Product{
		SKU:         dao.Sku,
		Name:        dao.Name,
		Brand:       dao.Brand,
		Size:        dao.Size,
		Price:       dao.Price,
		Image:       stringsToUrl([]string{dao.ImageURL})[0],
		OtherImages: stringsToUrl(dao.OtherImages),
	}
	return &updatedProduct, nil
}

type updateProductDAO struct {
	Sku         string   `bson:"sku,omitempty"`
	Name        string   `bson:"name,omitempty"`
	Brand       string   `bson:"brand,omitempty"`
	Size        string   `bson:"size,omitempty"`
	Price       float32  `bson:"price,omitempty"`
	ImageURL    string   `bson:"image,omitempty"`
	OtherImages []string `bson:"other,omitempty"`
}
