package mongodb

import (
	"context"
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/a.vandam/crud-challenge/internal/domain/entities"
	"gitlab.com/a.vandam/crud-challenge/internal/logger"
)

func TestGetProductBySKUAdapter_GetProducts(t *testing.T) {
	testCase := getProductsAdapterTestCase{
		testname: "fetch OK of one product",
		ctxArg:   context.TODO(),
		clientReturned: []getProductDAO{
			{
				Sku:         "SKU 234",
				Name:        "product name",
				Brand:       "example brand",
				Size:        "23",
				Price:       100.00,
				ImageURL:    "http://test-image.com",
				OtherImages: []string{"http://test-1.com", "http://test-2.com"},
			},
		},
		clientError: nil,
		expected: []entities.Product{
			{
				SKU:         "SKU 234",
				Name:        "product name",
				Brand:       "example brand",
				Size:        "23",
				Price:       100.00,
				Image:       stringsToUrl([]string{"http://test-image.com"})[0],
				OtherImages: stringsToUrl([]string{"http://test-1.com", "http://test-2.com"}),
			},
		},
	}
	testCase.Run(t)
}

func TestGetProductsAdapter_DBError(t *testing.T) {
	testCase := getProductsAdapterTestCase{
		testname:       "fetch failed due to with error",
		ctxArg:         context.TODO(),
		clientReturned: []getProductDAO{},
		clientError:    errors.New("a generic db error"),
		expected:       []entities.Product{},
		expectedErrMsg: "a generic db error",
	}
	testCase.Run(t)
}

type getProductsAdapterTestCase struct {
	testname       string
	ctxArg         context.Context
	clientReturned []getProductDAO
	clientError    error
	expected       []entities.Product
	expectedErrMsg string
}

func (tc getProductsAdapterTestCase) Run(t *testing.T) {
	t.Run(tc.testname, func(t *testing.T) {

		logFactory := logger.LogFactory{LogLevel: "info"}
		logger := logFactory.CreateLog("debug")
		adapter := GetProductsAdapter{
			Database:   "mock",
			Collection: "collection",
			Client: &dummyClientWrapper{
				getProdsResponse: tc.clientReturned,
				mockedError:      tc.clientError,
			},
			Log: logger,
		}
		got, err := adapter.GetProducts(tc.ctxArg)
		if err != nil && err.Error() != tc.expectedErrMsg {
			assert.EqualError(t, err, tc.expectedErrMsg, "error don't match")
			return
		}
		if !assert.Equal(t, *got, tc.expected) {
			t.Errorf("GetProductsAdapter.GetProducts() = %v, want %v", *got, tc.expected)
			t.Fail()

		}
	})
}
