package mongodb

import (
	"context"
	"net/url"

	"gitlab.com/a.vandam/crud-challenge/internal/domain/entities"
	"gitlab.com/a.vandam/crud-challenge/internal/logger"
)

type CreateProductAdapter struct {
	Database   string
	Collection string
	Client     InsertProductWrapperContract
	Log        logger.LogContract
}

/*GetProductBySKU perfroms a find query with a filter and retuirns a pointer to the product found*/
func (a *CreateProductAdapter) CreateProduct(ctx context.Context, product *entities.Product) (*entities.Product, error) {
	a.Log.Debug("trying to create product in db : %v, and collection: %v", a.Database, a.Collection)
	productObject := *product
	productToCreate := createProductDAO{
		Sku:         productObject.SKU,
		Name:        productObject.Name,
		Brand:       productObject.Brand,
		Size:        productObject.Size,
		Price:       productObject.Price,
		Image:       productObject.Image.String(),
		OtherImages: productObject.ImagesToString(),
	}
	daoPtr, err := a.Client.InsertOne(ctx, a.Database, a.Collection, &productToCreate)
	if err != nil {
		a.Log.Error("adapter found an error")
		return &entities.Product{}, err
	}
	dao := *daoPtr
	createdProduct := entities.Product{
		SKU:         dao.Sku,
		Name:        dao.Name,
		Brand:       dao.Brand,
		Size:        dao.Size,
		Price:       dao.Price,
		Image:       url.URL{Path: dao.Image},
		OtherImages: stringsToUrl(dao.OtherImages),
	}
	return &createdProduct, nil
}

type createProductDAO struct {
	Sku         string   `bson:"sku,omitempty"`
	Name        string   `bson:"name,omitempty"`
	Brand       string   `bson:"brand,omitempty"`
	Size        string   `bson:"size,omitempty"`
	Price       float32  `bson:"price,omitempty"`
	Image       string   `bson:"image,omitempty"`
	OtherImages []string `bson:"other,omitempty"`
}
