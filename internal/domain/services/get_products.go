package services

import (
	"context"

	"gitlab.com/a.vandam/crud-challenge/internal/domain/entities"
	"gitlab.com/a.vandam/crud-challenge/internal/domain/ports"
	"gitlab.com/a.vandam/crud-challenge/internal/logger"
)

/*GetProductsService holds the dependencies needed for the service to work*/
type GetProductsService struct {
	Port ports.GetProductsPort
	Log  logger.LogContract
}

/*GetProducts stores the service definition to get all products*/
func (svc GetProductsService) GetProducts(reqCtx context.Context) (*[]entities.Product, error) {
	svc.Log.Info("retrieving all products")
	prods, err := svc.Port.GetProducts(reqCtx)
	if err != nil {
		svc.Log.Error("port error while trying to GetProducts:%v", err)
		return &[]entities.Product{}, err
	}

	return prods, err
}
