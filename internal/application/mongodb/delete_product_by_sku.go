package mongodb

import (
	"context"

	"gitlab.com/a.vandam/crud-challenge/internal/logger"
	"go.mongodb.org/mongo-driver/bson"
)

type DeleteProductAdapter struct {
	Database   string
	Collection string
	Client     DeleteProductWrapperContract
	Log        logger.LogContract
}

/*GetProductBySKU perfroms a find query with a filter and retuirns a pointer to the product found*/
func (a *DeleteProductAdapter) DeleteProductBySKU(ctx context.Context, sku string) error {
	a.Log.Debug("trying to delete products from db: %v, and collection: %v, using sku =%v", a.Database, a.Collection, sku)
	filter := bson.D{{"sku", sku}}
	err := a.Client.DeleteOne(ctx, a.Database, a.Collection, filter)
	if err != nil {
		a.Log.Error("adapter found an error: %v", err)
	}
	return err

}
