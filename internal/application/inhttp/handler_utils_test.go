package inhttp

import (
	"errors"
	"net/url"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/a.vandam/crud-challenge/internal/domain/entities"
)

func TestWrapErrAsJson(t *testing.T) {
	testCases := []struct {
		testName string
		err      error
		expected string
	}{
		{
			testName: "a common error1 embedded as json",
			err:      errors.New("failed to process sth"),
			expected: `{"error":"failed to process sth", "resources":{}}`,
		},
	}

	for i, testCase := range testCases {
		t.Logf("testing parsing in method ParseInterfaceToInt32: test nmber: %v - %v", i, testCase.testName)
		valueObtained := wrapErrAsJSON(testCase.err)
		if !assert.Equal(t, testCase.expected, valueObtained, "diff between expected ( %v ) and obtained ( %v )", testCase.expected, valueObtained) {
			t.FailNow()
		}
		t.Logf("--- OK --- test number %v - testName: %v ---", i, testCase.testName)
	}
}

func TestMapDTOToJson(t *testing.T) {
	testCases := []struct {
		testName          string
		dto               productJSONRequest
		expected          entities.Product
		expectedErrString string
	}{
		{
			testName: "map request to product",
			dto: productJSONRequest{
				SKU:         "sku",
				Name:        "name",
				Brand:       "brand",
				Size:        "123",
				Image:       "http://localhost.com",
				Price:       123,
				OtherImages: []string{"http://localhost.com", "https://google.com"},
			},
			expected: entities.Product{
				SKU:         "sku",
				Name:        "name",
				Brand:       "brand",
				Size:        "123",
				Image:       stringsToUrls("http://localhost.com")[0],
				Price:       123,
				OtherImages: stringsToUrls("http://localhost.com", "https://google.com"),
			},
		},
		{
			testName: "map request to product with missing sku",
			dto: productJSONRequest{
				Name:        "name",
				Brand:       "brand",
				Size:        "123",
				Image:       "http://localhost.com",
				Price:       123,
				OtherImages: []string{"http://localhost.com", "https://google.com"},
			},
			expected:          entities.Product{},
			expectedErrString: `required field \"sku\" is missing`,
		}, {
			testName: "map request to product with missing brand",
			dto: productJSONRequest{
				SKU:         "sku",
				Name:        "name",
				Size:        "123",
				Image:       "http://localhost.com",
				Price:       123,
				OtherImages: []string{"http://localhost.com", "https://google.com"},
			},
			expected:          entities.Product{},
			expectedErrString: `required field \"brand\" is missing`,
		}, {
			testName: "map request to product with missing image",
			dto: productJSONRequest{
				SKU:         "sku",
				Name:        "name",
				Brand:       "brand",
				Size:        "123",
				Price:       123,
				OtherImages: []string{"http://localhost.com", "https://google.com"},
			},
			expected:          entities.Product{},
			expectedErrString: `required field \"image\" is missing`,
		}, {
			testName: "map request to product with missing price",
			dto: productJSONRequest{
				SKU:         "sku",
				Name:        "name",
				Brand:       "brand",
				Size:        "123",
				Image:       "http://localhost.com",
				OtherImages: []string{"http://localhost.com", "https://google.com"},
			},
			expected:          entities.Product{},
			expectedErrString: `required field \"price\" is missing`,
		},
		{
			testName: "map request to product with bad url",
			dto: productJSONRequest{
				SKU:         "sku",
				Name:        "name",
				Brand:       "brand",
				Size:        "123",
				Image:       "faku url",
				Price:       123,
				OtherImages: []string{"http://localhost.com", "https://google.com"},
			},
			expected:          entities.Product{},
			expectedErrString: `parse "faku url": invalid URI for request`,
		},
	}

	for i, testCase := range testCases {
		t.Logf("testing map request to product: test nmber: %v - %v", i, testCase.testName)
		valueObtained, err := mapProductJsonRequestToEntity(&testCase.dto)
		if testCase.expectedErrString == "" {
			if !assert.NoError(t, err, "no error expected, still got one: %v", err) {
				t.FailNow()
			}
		} else {
			if !assert.EqualError(t, err, testCase.expectedErrString, "diff between expected ( %v )and got ( %v ) error", testCase.expectedErrString, err) {
				t.FailNow()
			}
		}
		if !assert.Equal(t, testCase.expected, *valueObtained, "diff between expected ( %v ) and obtained ( %v )", testCase.expected, valueObtained) {
			t.FailNow()
		}

		t.Logf("--- OK --- test number %v - testName: %v ---", i, testCase.testName)
	}
}

func stringsToUrls(urls ...string) []url.URL {
	urlsSlice := make([]url.URL, len(urls))
	for i, urlString := range urls {
		var urlObject url.URL
		urlObject.UnmarshalBinary([]byte(urlString))
		urlsSlice[i] = urlObject
	}
	return urlsSlice
}
