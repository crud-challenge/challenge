PROJECT_NAME := "crud-challenge"
PKG := "gitlab.com/a.vandam/$(PROJECT_NAME)"
PKG_LIST := $(shell go list ${PKG}/... | grep -v /vendor/)
GO_FILES := $(shell find . -name '*.go' | grep -v /vendor/ | grep -v _test.go)
MAIN_LOCATION := "./cmd/main.go"


.PHONY: lint clean start-test-env stop-test-env build help test

lint: ## Lint the files
	@golint -set_exit_status ${PKG_LIST}

clean: ## Remove previous build
	@rm -f $(PROJECT_NAME)

start-db:
	@docker-compose up -d database

start-service:
	@docker-compose build crud-challenge && docker-compose up -d crud-challenge && docker logs crud-challenge

start-test-env:
	@make clean && docker-compose up

stop-test-env:
	@docker-compose down

build:
	@go build ${MAIN_LOCATION}

test:
	@go test ./internal/... -cover