package services

import (
	"context"

	"gitlab.com/a.vandam/crud-challenge/internal/domain/entities"
)

/*GetProductBySKUServiceContract is the contract that service must respect to be injected in the handler*/
type GetProductBySKUServiceContract interface {
	GetProductBySKU(ctx context.Context, sku string) (*entities.Product, error)
}

/*GetProductsServiceContract is the contract that service must respect to be injected in the handler*/
type GetProductsServiceContract interface {
	GetProducts(ctx context.Context) (*[]entities.Product, error)
}

/*UpdateProductServiceContract is the contract that service must respect to be injected in the handler*/
type UpdateProductServiceContract interface {
	UpdateProduct(ctx context.Context, prod *entities.Product) (*entities.Product, error)
}

/*DeleteProductServiceContract is the contract that service must respect to be injected in the handler*/
type DeleteProductServiceContract interface {
	DeleteProduct(ctx context.Context, sku string) error
}
type CreateProductServiceContract interface {
	CreateProduct(ctx context.Context, prod *entities.Product) (*entities.Product, error)
}
