package mongodb

import (
	"context"

	"gitlab.com/a.vandam/crud-challenge/internal/domain/entities"
	"gitlab.com/a.vandam/crud-challenge/internal/logger"
	"go.mongodb.org/mongo-driver/bson"
)

type GetProductsAdapter struct {
	Database   string
	Collection string
	Client     GetProductsWrapperContract
	Log        logger.LogContract
}

/*GetProducts: TODO*/
func (a *GetProductsAdapter) GetProducts(ctx context.Context) (*[]entities.Product, error) {
	a.Log.Debug("trying to get products from db: %v, and collection: %v", a.Database, a.Collection)
	filter := bson.D{}
	daosPtr, err := a.Client.FindProducts(ctx, a.Database, a.Collection, filter)
	if err != nil {
		a.Log.Error("adapter found an error")
		return &[]entities.Product{}, err
	}
	daos := *daosPtr
	products := make([]entities.Product, len(daos))
	for i, dao := range daos {
		products[i] = entities.Product{
			SKU:         dao.Sku,
			Name:        dao.Name,
			Brand:       dao.Brand,
			Size:        dao.Size,
			Price:       dao.Price,
			Image:       stringsToUrl([]string{dao.ImageURL})[0],
			OtherImages: stringsToUrl(dao.OtherImages),
		}
	}
	return &products, nil
}
