FROM  golang:1.18.1-stretch

WORKDIR /go/src/app

COPY go.mod . 
COPY go.sum .
RUN go mod download

ADD . .

CMD ["echo","starting application via docker"] 
RUN `go mod tidy \
  && go build ./cmd/main.go`

CMD ["./main"]

