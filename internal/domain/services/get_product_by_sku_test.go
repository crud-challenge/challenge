package services

import (
	"context"
	"errors"
	"net/url"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/a.vandam/crud-challenge/internal/domain/entities"
	"gitlab.com/a.vandam/crud-challenge/internal/logger"
)

func TestGetAProduct(t *testing.T) {
	testCase := getProdBySKUTestCase{

		testName: "retrieve one product",
		sku:      "sku 1234",
		returnedProds: entities.Product{
			SKU:         "sku 1234",
			Name:        "product name",
			Brand:       "example brand",
			Size:        "23",
			Price:       100.00,
			Image:       url.URL{Path: "http://test-image.com"},
			OtherImages: []url.URL{url.URL{Path: "http://test-1.com"}, url.URL{Path: "http://test-1.com"}},
		},
		expectedProd: entities.Product{
			SKU:         "sku 1234",
			Name:        "product name",
			Brand:       "example brand",
			Size:        "23",
			Price:       100.00,
			Image:       url.URL{Path: "http://test-image.com"},
			OtherImages: []url.URL{url.URL{Path: "http://test-1.com"}, url.URL{Path: "http://test-1.com"}},
		},
		expectedErr: "",
	}

	t.Run(testCase.testName, testCase.testAndAssert)
}

func TestGetProductBySKU_PortError(t *testing.T) {
	testCase := getProdBySKUTestCase{

		testName:        "port error while getting a product",
		sku:             "error sku",
		returnedProds:   entities.Product{},
		errorPortInMock: errors.New("generic db error"),
		expectedProd:    entities.Product{},
		expectedErr:     "generic db error",
	}

	t.Run(testCase.testName, testCase.testAndAssert)
}

// End test cases

type getProdBySKUTestCase struct {
	testName        string
	sku             string
	returnedProds   entities.Product
	errorPortInMock error
	expectedProd    entities.Product
	expectedErr     string
}

func (testCase getProdBySKUTestCase) testAndAssert(t *testing.T) {
	t.Logf("testing function")

	/**---------------------- FUNCTION UNDER TEST -----------------------**/
	/*Dependencies*/
	mockedPort := mockedGetProductByIDPort{
		products: testCase.returnedProds,
		err:      testCase.errorPortInMock,
	}
	loggerFactory := logger.LogFactory{LogLevel: "INFO"}
	log := loggerFactory.CreateLog("")
	svc := GetProductBySKUService{
		Port: mockedPort,
		Log:  log,
	}
	/*END Dependencies*/
	testCtx := context.Background()
	product, err := svc.GetProductBySKU(testCtx, testCase.sku)
	/**---------------------- END FUNCTION UNDER TEST -----------------------**/
	if !assert.Equal(t, &testCase.expectedProd, product, "difference in value expected (%v) and obtained (%v)", testCase.expectedProd, product) {
		t.Fail()
	}
	if testCase.expectedErr != "" && err == nil {
		t.Logf("test failed as the function did not return an expected error: %v vs %v", err, testCase.expectedErr)
		t.FailNow()
	}
	if testCase.expectedErr == "" && err != nil {
		t.Logf("test failed as the function returned an error when it shouldn't: %v", err)
		t.FailNow()
	}
	if testCase.expectedErr != "" && err != nil {
		//comparing errors
		if !assert.EqualErrorf(t, err, testCase.expectedErr, "function returned an unexpected error: expected: %v vs found: %v", testCase.expectedErr, err) {
			t.FailNow()
		}
	}

	t.Logf("OK!!!! - test case:  %v  - OK!!!!", testCase.testName)
}

/*Mocking*/
type mockedGetProductByIDPort struct {
	products entities.Product
	err      error
}

func (mock mockedGetProductByIDPort) GetProductBySKU(ctx context.Context, sku string) (*entities.Product, error) {
	return &mock.products, mock.err
}
