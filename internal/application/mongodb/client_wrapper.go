package mongodb

import (
	"context"

	"gitlab.com/a.vandam/crud-challenge/internal/infrastructure/configs"
	"gitlab.com/a.vandam/crud-challenge/internal/logger"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

/*ClientWrapper acts as a wrapper that embeds the mongo client, leaving business logic or more query related logic to it's invokeres (the adapter, for ex)*/
type ClientWrapper struct {
	client *mongo.Client
	Log    logger.LogContract
}

/*Connect basically sets up a connection to the DB using DB conn required params. Must be pinged later to check it's feasibility.*/
func (a ClientWrapper) Connect(ctx context.Context, configs *configs.ProductsDBConfigurations) (*ClientWrapper, error) {
	a.Log.Info("creating URI for DB connection")
	dbType := "mongodb://"
	userAndPwdString := configs.MongoDatabaseUsername + ":" + configs.MongoDatabasePassword
	hostAndPort := configs.MongoDatabaseHost + ":" + configs.MongoDatabasePort
	dbNamePath := "/" + configs.MongoDatabaseName
	authSource := "?authSource=" + configs.MongoAuthSource
	dbURI := dbType + userAndPwdString + "@" + hostAndPort + dbNamePath + authSource
	a.Log.Debug("db Connection URI: %v", dbURI)

	a.Log.Debug("creating client")
	client, err := mongo.Connect(context.TODO(), options.Client().ApplyURI(dbURI).SetDirect(true))

	if err != nil {
		a.Log.Error("mongo db conn error: %v", err)
		return &ClientWrapper{}, err
	}
	a.Log.Info("connected to productgs mongo db!")
	a.client = client
	a.Log.Info("products db mongo db connector connected succesfully")
	return &a, nil
}

/*Ping allows to test if the connection has been established succesfully*/
func (a ClientWrapper) Ping() error {
	a.Log.Debug("pinging db...")
	return a.client.Ping(nil, nil)
}

/*Disconnect disconnects the Client from the DB*/
func (a ClientWrapper) Disconnect(ctx context.Context) error {
	a.Log.Debug("disconnecting from DB...")
	return a.client.Disconnect(ctx)
}
