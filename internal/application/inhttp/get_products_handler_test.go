package inhttp

import (
	"context"
	"encoding/json"
	"errors"
	"net/http"
	"net/http/httptest"
	"os"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/a.vandam/crud-challenge/internal/domain/entities"
	"gitlab.com/a.vandam/crud-challenge/internal/logger"
)

func TestGetProducts_OK(t *testing.T) {
	testCase := getProductsHandlerTestCase{
		testName: "request ok",
		path:     "http://svctest/products",
		bodyFile: "",
		verb:     "GET",
		svcProdsResponse: []entities.Product{
			{
				SKU:         "SKU 234",
				Name:        "product name",
				Brand:       "example brand",
				Size:        "23",
				Price:       100.00,
				Image:       stringsToUrls("http://test-image.com")[0],
				OtherImages: stringsToUrls("http://test-1.com", "http://test-2.com")},
			{
				SKU:         "SKU 2345",
				Name:        "product name 2",
				Brand:       "example brand 2",
				Size:        "232",
				Price:       100.00,
				Image:       stringsToUrls("http://test-image2.com")[0],
				OtherImages: stringsToUrls("http://test-12.com", "http://test-22.com")},
		},
		expectedCode:     200,
		expectedJSONResp: "response_bodies/get_products/get_products_ok.json",
	}

	t.Run(testCase.testName, testCase.testAndAssert)
}
func TestGetProducts_NoProducts(t *testing.T) {
	testCase := getProductsHandlerTestCase{
		testName:         "request ok but no content",
		path:             "http://svctest/products",
		bodyFile:         "",
		verb:             "GET",
		svcProdsResponse: []entities.Product{},
		svcErrResponse:   nil,
		expectedCode:     http.StatusNoContent,
		expectedJSONResp: "",
	}

	t.Run(testCase.testName, testCase.testAndAssert)
}
func TestGetProducts_SvcError(t *testing.T) {
	testCase := getProductsHandlerTestCase{
		testName:         "request ok, service error",
		path:             "http://svctest/products",
		bodyFile:         "",
		verb:             "GET",
		svcProdsResponse: []entities.Product{},
		svcErrResponse:   errors.New("service error"),
		expectedCode:     http.StatusInternalServerError,
		expectedJSONResp: "response_bodies/get_products/get_products_svcerror.json",
	}

	t.Run(testCase.testName, testCase.testAndAssert)
}

type getProductsHandlerTestCase struct {
	testName         string
	path             string
	verb             string
	bodyFile         string
	svcProdsResponse []entities.Product
	svcErrResponse   error
	expectedCode     int
	expectedJSONResp string
}

func (testCase getProductsHandlerTestCase) testAndAssert(t *testing.T) {
	t.Logf("testing function")

	/**---------------------- FUNCTION UNDER TEST -----------------------**/
	/*Dependencies*/
	mockedSvc := dummyGetProductsPort{
		product: &testCase.svcProdsResponse,
		svcErr:  testCase.svcErrResponse,
	}
	loggerFactory := logger.LogFactory{LogLevel: "DEBUG"}
	log := loggerFactory.CreateLog("")
	dependencies := GetProductsHandlerDeps{mockedSvc, log}
	/*END Dependencies*/
	// Test function //
	handlerFunc := CreateGetProductsHandlerFunc(dependencies)

	/*Create test request and server*/
	var req *http.Request

	if testCase.bodyFile == "" {
		req = httptest.NewRequest(testCase.verb, testCase.path, nil)
	} else {
		bodyToSend, fileReadErr := os.ReadFile(testCase.bodyFile)
		if fileReadErr != nil {
			t.Logf("error while opening json request file: %v", fileReadErr)
			t.FailNow()
			return
		}
		reader := strings.NewReader(string(bodyToSend))
		req = httptest.NewRequest(testCase.verb, testCase.path, reader)

	}
	responseWriter := httptest.NewRecorder()
	handlerFunc(responseWriter, req)
	/*END Create test request and server*/
	/**---------------------- END FUNCTION UNDER TEST -----------------------**/
	receivedCode := responseWriter.Result().StatusCode
	if !assert.Equal(t, testCase.expectedCode, receivedCode, "difference in http code expected (%v) and obtained (%v)", testCase.expectedCode, receivedCode) {
		t.FailNow()
		return
	}
	var expectedBody []byte
	var bodyNotFoundErr error
	if testCase.expectedJSONResp != "" {
		expectedBody, bodyNotFoundErr = os.ReadFile(testCase.expectedJSONResp)
	} else {
		expectedBody = nil
		bodyNotFoundErr = nil
	}
	if bodyNotFoundErr != nil {
		t.Logf("json file that stores the expected body has not been found: %v", bodyNotFoundErr)
		t.FailNow()
		return
	}
	var expectedBodyAsMap, receivedBodyAsMap EmbeddingMultipleResourcesJSONResponse
	json.Unmarshal(expectedBody, &expectedBodyAsMap)
	receivedBody := responseWriter.Body.Bytes()
	json.Unmarshal(receivedBody, &receivedBodyAsMap)

	if !assert.Equal(t, expectedBodyAsMap, receivedBodyAsMap, "difference in body expected (%v) and obtained (%v)", expectedBodyAsMap, receivedBodyAsMap) {
		t.FailNow()
		return
	}

	t.Logf("OK!!!! - test case:  %v  - OK!!!!", testCase.testName)
}

type dummyGetProductsPort struct {
	product *[]entities.Product
	svcErr  error
}

func (mock dummyGetProductsPort) GetProducts(ctx context.Context) (*[]entities.Product, error) {
	return mock.product, mock.svcErr
}
