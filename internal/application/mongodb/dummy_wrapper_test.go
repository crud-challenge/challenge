package mongodb

import (
	"context"

	"gitlab.com/a.vandam/crud-challenge/internal/infrastructure/configs"
	"go.mongodb.org/mongo-driver/bson"
)

type dummyClientWrapper struct {
	createProdResponse   createProductDAO
	updateProdResponse   updateProductDAO
	getProdBYSKUResponse getProductDAO
	getProdsResponse     []getProductDAO
	mockedError          error
}

func (dummy dummyClientWrapper) Connect(ctx context.Context, configs *configs.ProductsDBConfigurations) (ClientWrapperContract, error) {
	return dummy, nil
}
func (dummy dummyClientWrapper) Ping() error {
	return dummy.mockedError
}
func (dummy dummyClientWrapper) Disconnect(ctx context.Context) error {
	return dummy.mockedError
}
func (dummy dummyClientWrapper) FindProducts(ctx context.Context, databaseName string, collectionName string, filter bson.D) (*[]getProductDAO, error) {
	return &dummy.getProdsResponse, dummy.mockedError
}
func (dummy dummyClientWrapper) FindProduct(ctx context.Context, databaseName string, collectionName string, filter bson.D) (*getProductDAO, error) {
	return &dummy.getProdBYSKUResponse, dummy.mockedError
}
func (dummy dummyClientWrapper) DeleteOne(ctx context.Context, databaseName string, collectionName string, filter bson.D) error {
	return dummy.mockedError
}
func (dummy dummyClientWrapper) InsertOne(ctx context.Context, databaseName string, collectionName string, product *createProductDAO) (*createProductDAO, error) {
	return &dummy.createProdResponse, dummy.mockedError
}
func (dummy dummyClientWrapper) UpdateOne(ctx context.Context, databaseName string, collectionName string, productPtr *updateProductDAO, filter bson.D) (*updateProductDAO, error) {
	return &dummy.updateProdResponse, dummy.mockedError
}
