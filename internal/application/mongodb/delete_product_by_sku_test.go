package mongodb

import (
	"context"
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/a.vandam/crud-challenge/internal/logger"
)

func TestDeleteProductBySKUAdapter_DeleteProduct(t *testing.T) {
	testCase := deleteProductBySKUTestCase{
		testname:     "delete one product",
		ctxArg:       context.TODO(),
		wrapperError: nil,
	}
	testCase.Run(t)
}

func TestDeleteProductBySKUAdapter_DBError(t *testing.T) {
	testCase := deleteProductBySKUTestCase{
		testname:       "delete failed due to db error",
		ctxArg:         context.TODO(),
		wrapperError:   errors.New("a generic db error"),
		expectedErrMsg: "a generic db error",
	}
	testCase.Run(t)
}

type deleteProductBySKUTestCase struct {
	testname       string
	ctxArg         context.Context
	skuUsed        string
	wrapperError   error
	expectedErrMsg string
}

func (tc deleteProductBySKUTestCase) Run(t *testing.T) {
	t.Run(tc.testname, func(t *testing.T) {

		logFactory := logger.LogFactory{LogLevel: "info"}
		logger := logFactory.CreateLog("debug")
		adapter := DeleteProductAdapter{
			Database:   "mock",
			Collection: "collection",
			Client: &dummyClientWrapper{
				mockedError: tc.wrapperError,
			},
			Log: logger,
		}
		err := adapter.DeleteProductBySKU(tc.ctxArg, tc.skuUsed)
		if err != nil && err.Error() != tc.expectedErrMsg {
			assert.EqualError(t, err, tc.expectedErrMsg, "error don't match")
			return
		}
	})
}
