package mongodb

import (
	"context"

	"gitlab.com/a.vandam/crud-challenge/internal/infrastructure/configs"
	"go.mongodb.org/mongo-driver/bson"
)

/*ClientWrapperContract is the contract that all client's inyected upon the products adapter must comply.*/
type ClientWrapperContract interface {
	Connect(ctx context.Context, configs *configs.ProductsDBConfigurations) (ClientWrapperContract, error)
	Ping() error
	Disconnect(ctx context.Context) error
}

type GetProductsWrapperContract interface {
	FindProducts(ctx context.Context, databaseName string, collectionName string, filter bson.D) (*[]getProductDAO, error)
	FindProduct(ctx context.Context, databaseName string, collectionName string, filter bson.D) (*getProductDAO, error)
}

type DeleteProductWrapperContract interface {
	DeleteOne(ctx context.Context, databaseName string, collectionName string, filter bson.D) error
}
type InsertProductWrapperContract interface {
	InsertOne(ctx context.Context, databaseName string, collectionName string, product *createProductDAO) (*createProductDAO, error)
}
type UpdateProductWrapperContract interface {
	UpdateOne(ctx context.Context, databaseName string, collectionName string, productPtr *updateProductDAO, filter bson.D) (*updateProductDAO, error)
}
